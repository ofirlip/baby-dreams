﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class StateEvents : StateMachineBehaviour
    {
        private bool _wasEnter;
        private bool _wasExit;

        public string Name;

        private void OnEnter()
        {
            _wasEnter = true;
            _wasExit = false;            
        }

        public override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
        {
            OnEnter();
        }

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            OnEnter();
        }

        private void OnExit()
        {
            _wasEnter = false;
            _wasExit = true;            
        }

        public override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
        {
            OnExit();
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            OnExit();
        }

        public IEnumerator WaitExit()
        {

            while (!_wasEnter)
			{
                yield return null;
			}
            while (!_wasExit)
			{
				yield return null;
			}

        }

        public IEnumerator WaitEnter()
        {
            while (!_wasEnter)
                yield return null;
        }
    }
}
