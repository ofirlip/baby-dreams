﻿using Assets.Scripts.Utils;
using Assets.Scripts.Utils.GameEvents;
using Assets.Scripts.Utils.Tweens;
using UnityEngine;

namespace Assets.Scripts
{
    public class CameraController : CommandMonoBehaviour
    {
        private float _cameraDefaultSize;
        private Transform _friendCameraPivot;
        private bool _isMovingToFriendCameraPivot;
        private Transform _cameraTransform;

        public Camera Camera;
        public Transform Butterfly;
        public float MinY;
        public float MaxY;
        public float MovingSpeed;
        public float MinMovingSpeed;

        public float FocusSpeed;

        public void Awake()
        {
            Application.targetFrameRate = 60;
            _cameraDefaultSize = Camera.orthographicSize;
            _cameraTransform = Camera.transform;
        }

		public void MoveToFriendCameraPivot(Transform cameraPivot)
		{
			_isMovingToFriendCameraPivot = true;
			_friendCameraPivot = cameraPivot;
		}

        public void FocusCameraOnFriend(float cameraFocusSize)
        {
			CameraTween.ChangeSize(Camera, cameraFocusSize, FocusSpeed).SetEase(Ease.InOutCubic);
        }

        public void UnFocusCamera()
        {
			CameraTween.ChangeSize(Camera, _cameraDefaultSize, FocusSpeed)
				.SetEase(Ease.InOutCubic)
					.OnFinish(() => {_isMovingToFriendCameraPivot = false;});
        }

        public override void Update()
        {
            base.Update();

            if (_isMovingToFriendCameraPivot)
            {
                MoveToTransform(_friendCameraPivot, true);
            }
            else
            {
				MoveToTransform(Butterfly, false);
            }
        }

        private void MoveToTransform(Transform tr, bool canMoveBack)
        {
            Vector3 targetPosition = new Vector3(tr.position.x, tr.position.y, _cameraTransform.position.z);
            if (!canMoveBack)
			{
                targetPosition.x = Mathf.Max(targetPosition.x, _cameraTransform.position.x);
			}
            targetPosition = ControlMinMaxYPosition(targetPosition);

            if (Vector2.Distance(_cameraTransform.position, targetPosition) > 0.1f)
            {
                float speed = Mathf.Max(Vector3.Distance(_cameraTransform.position, targetPosition)*MovingSpeed, MinMovingSpeed);
                Vector3 dir = (targetPosition - _cameraTransform.position).normalized;
                _cameraTransform.Translate(dir*speed*Time.deltaTime);
            }
        }

        private Vector3 ControlMinMaxYPosition(Vector3 targetPosition)
        {
            if (targetPosition.y < MinY)
			{
                targetPosition.y = MinY;
			}
            if (targetPosition.y > MaxY)
			{
                targetPosition.y = MaxY;
			}
            return targetPosition;
        }

    }
}
