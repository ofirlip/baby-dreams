﻿using System;
using System.Collections;
using System.Linq;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.GameEvents;
using UnityEngine;
using System.Collections.Generic;
using Prime31;
using ChartboostSDK;

namespace Assets.Scripts
{
	public class UIManager : CommandMonoBehaviour {

		public GameObject	UIParentsBarrier;
		public bool			UIOpened;

		public BedRoomButterflyManager	BedroomManager;

		public GameObject	SoundPlayButton;
		public GameObject	SoundMuteButton;

		public GameObject	ParentsWindow;
		public GameObject	ParentsMovable;

		public GameObject	RateUsWindow;
		public GameObject[]	RateUsStars;
		public GameObject 	RateUsBtnDisabled;
		public GameObject	RateUsBtnEnabled;

		public GameObject	MoreAppsIcon;
		public float 		MoreAppsTimer;
		private float 		moreAppsTimePassed;
		public GameObject	PromoWindow;

		public GameObject	FeedbackWindow;
		private int 		selectedRateStars;

		public GameObject	HomeWindow;

		public GameObject	RestoreBtn;
		public GameObject	BuyFullGameBtn;
		public GameObject[]	BlockingFencesObjs;
		public GameObject	UILoadingScreen;

		private Vector3		MoreAppsIconStartingPos = new Vector3(7.4f,-8.6f,0f);
		private Vector3[]  	VectorsCoordinatePositions = new [] { new Vector3(0f,33f,0f), 
																	new Vector3(0f,16.5f,0f), 
																	new Vector3(0f,0f,0f)  };//0 - Top  1- Middle 2- Down
        private bool showAdsOnce = true;
		public enum StartingPos
		{
			Top,
			Middle,
			Down
		}

		void Start()
        {
            Chartboost.cacheInterstitial(CBLocation.Default);
            Chartboost.didCacheInterstitial += didCacheInterstitial;

            UIOpened = false;
			selectedRateStars = 5;
			moreAppsTimePassed= 0;
			if(PlayerPrefs.GetString("BgSoundMuted","No")=="Yes")
			{
				SoundPlayButton.SetActive(false);
				SoundMuteButton.SetActive(true);
			}
			else 
			{
				SoundPlayButton.SetActive(true);
				SoundMuteButton.SetActive(false);
			}

//			if(PlayerPrefs.GetString("AlreadyBoughtGame","No") == "Yes")
//			{
//				RestoreBtn.SetActive(false);
//				BuyFullGameBtn.SetActive(false);
//				for(int i=0;i<BlockingFencesObjs.Length;i++)
//					BlockingFencesObjs[i].SetActive(false);
//			}
		}

        void didCacheInterstitial(CBLocation location)
        {
            if(location == CBLocation.Default && showAdsOnce)
                ShowInterstitial();
        }

        public void ShowInterstitial()
        {
            Debug.Log("Has " + Chartboost.hasInterstitial(CBLocation.Default));
            if (Chartboost.hasInterstitial(CBLocation.Default))
            {
                Chartboost.showInterstitial(CBLocation.Default);
                showAdsOnce = false;
            }
        }

        void Update()
		{
			moreAppsTimePassed += Time.deltaTime;
			if(moreAppsTimePassed>MoreAppsTimer && MoreAppsIcon.transform.localPosition.y != -5.8f)
			{
				MoreAppsIcon.transform.localPosition += new Vector3(0,5f,0)*Time.deltaTime;
				MoreAppsIcon.transform.localPosition = new Vector3(MoreAppsIcon.transform.localPosition.x,
				                                                   Mathf.Clamp(MoreAppsIcon.transform.localPosition.y,-8.6f,-5.8f),
				                                                   MoreAppsIcon.transform.localPosition.z);
			}

			if (Input.GetKeyUp(KeyCode.F1))
			{
//				UIParentsBarrier.gameObject.GetComponent<ParentsBarrier>().GenerateSwipeDirection(ParentsBarrier.BarrierTypes.OptionsScreen);
//				ShowUILabels(UIParentsBarrier,StartingPos.Down);
				WaitBarrier(1f,ParentsBarrier.BarrierTypes.BuyFullVersion);
			}
			else if(Input.GetKeyUp(KeyCode.F2))
			{
				StartCoroutine(BedroomManager.ReturnToBedroom());
			}
		}

		public void OnDrag( DragGesture gesture )
		{

			if(gesture.Selection != null )
			{
				if( gesture.Selection.name == "Scrollable" && gesture.Phase != ContinuousGesturePhase.Ended )
				{
					ParentsMovable.transform.localPosition += new Vector3(0,gesture.DeltaMove.y/2,0)*Time.deltaTime;
					ParentsMovable.transform.localPosition = new Vector3(ParentsMovable.transform.localPosition.x,
					                                                     Mathf.Clamp(ParentsMovable.transform.localPosition.y,0,17f),
					                                                     ParentsMovable.transform.localPosition.z);
				}
			}
		}

		public void OnTap(TapGesture gesture)
		{
			if(gesture.Selection != null )
			{
//				Debug.Log(gesture.Selection.name );
				if(gesture.Selection.name == "Mute")
				{
					PlayerPrefs.SetString("BgSoundMuted","No");
					SoundPlayButton.SetActive(true);
					SoundMuteButton.SetActive(false);
					SoundsManager.Instance.AudioSourceBGs.volume = 0.2f;
					SoundsManager.Instance.AudioSourceBGs.UnPause();
				}
				else if(gesture.Selection.name == "Play")
				{
					PlayerPrefs.SetString("BgSoundMuted","Yes");
					SoundPlayButton.SetActive(false);
					SoundMuteButton.SetActive(true);
					SoundsManager.Instance.AudioSourceBGs.volume = 0f;
					SoundsManager.Instance.AudioSourceBGs.Pause();
				}
				else if(gesture.Selection.name == "HomeBG")
				{
					StartCoroutine(ReloadGame());
				}
				else if(gesture.Selection.name == "ParentsButton")
				{
					UIParentsBarrier.gameObject.GetComponent<ParentsBarrier>().GenerateSwipeDirection(ParentsBarrier.BarrierTypes.ParentsWindow);
					UIParentsBarrier.transform.position = new Vector3(-835.42f,333.03f,5);
					UIParentsBarrier.SetActive(true);
				}
				else if(gesture.Selection.name == "BackBTN")
				{
					MoreAppsIcon.transform.localPosition = MoreAppsIconStartingPos;
					moreAppsTimePassed = 0;
					HomeWindow.SetActive(true);
					GameObject.Find("MainMenuCamera").transform.localPosition = new Vector3(24.2f,-5.7f,-10f);
					ParentsWindow.SetActive(false);

				}
				else if(gesture.Selection.name == "PrivacyPolicyButton")
				{
					Application.OpenURL("http://libilabs.com/privacy/");
				}
				else if(gesture.Selection.name == "EmailButton" || gesture.Selection.name == "MailUs")
				{
					Debug.Log("Calling Mail");
				#if UNITY_IPHONE
					EtceteraBinding.showMailComposer("contact@libilabs.com", "Baby Dreams Feedback", string.Empty, false);
				#endif
				}
				else if (gesture.Selection.name == "RateUsSmallButton")
				{
					selectedRateStars = 5;
					for(int i=0;i<RateUsStars.Length;i++)
						RateUsStars[i].transform.Find("StarYellow").gameObject.SetActive(true);
					RateUsWindow.SetActive(true);
					RateUsBtnEnabled.SetActive(true);
				}
				else if (gesture.Selection.name == "CloseButtonRateUs" || gesture.Selection.name == "NotNow")
				{
					RateUsWindow.SetActive(false);
					FeedbackWindow.SetActive(false);
				}
				else if(gesture.Selection.name.Contains("Star"))
				{
					for (int i=0;i<RateUsStars.Length;i++)
					{
						if(gesture.Selection == RateUsStars[i])
							selectedRateStars = i+1;
					}
					for(int i=0;i<RateUsStars.Length;i++)
					{
						if(i<=selectedRateStars-1)
							RateUsStars[i].transform.Find("StarYellow").gameObject.SetActive(true);
						else RateUsStars[i].transform.Find("StarYellow").gameObject.SetActive(false);
					}
				}
				else if (gesture.Selection.name == "RateUsEnable")
				{
					if(selectedRateStars == 5)
						Application.OpenURL("https://itunes.apple.com/us/app/good-morning-good-night/id1049218708?mt=8");
					else 
					{
						RateUsWindow.SetActive(false);
						FeedbackWindow.SetActive(true);
					}
				}
				else if(gesture.Selection.name == "MoreAppsIcon")
				{
					PromoWindow.SetActive(true);
					GameObject.Find("MainMenuCamera").transform.position = new Vector3(-61.67f,11.86f,-10f);
					GameObject.Find("MainMenuCamera").GetComponent<Camera>().orthographicSize = 3.8f;
					HomeWindow.SetActive(false);

				}
				else if(gesture.Selection.name == "Close_BTN")
				{
					UIParentsBarrier.SetActive(false);
					this.transform.localScale = new Vector3(1f,1f,1f);
					
					if(GameObject.Find("MainCamera").GetComponent<Camera>().depth > 0)
						GameObject.Find("InputManager").GetComponent<InputManager>().enabled = true;
				}
				else if(gesture.Selection.name == "PlayNow2")
				{
					UIParentsBarrier.gameObject.GetComponent<ParentsBarrier>().GenerateSwipeDirection(ParentsBarrier.BarrierTypes.MoreApps);
					UIParentsBarrier.transform.localPosition = new Vector3(777.3f,-324.4f,5);
					UIParentsBarrier.SetActive(true);
				}
				else if(gesture.Selection.name == "BackToGoodMorning" || gesture.Selection.name == "ClosePromoAnimals")
				{
					MoreAppsIcon.transform.localPosition = MoreAppsIconStartingPos;
					moreAppsTimePassed = 0;
					HomeWindow.SetActive(true);
					GameObject.Find("MainMenuCamera").transform.localPosition = new Vector3(24.2f,-5.7f,-10f);
					GameObject.Find("MainMenuCamera").GetComponent<Camera>().orthographicSize = 4f;
					PromoWindow.SetActive(false);
				}
				else if(gesture.Selection.name == "BuyFullVersionButton")
				{
					SoundsManager.Play("BuyFullVersion2");
					UIParentsBarrier.gameObject.GetComponent<ParentsBarrier>().GenerateSwipeDirection(ParentsBarrier.BarrierTypes.ParentsWindow);
					UIParentsBarrier.transform.position = new Vector3(-835.42f,333.03f,5);
					UIParentsBarrier.SetActive(true);
				}
				else if(gesture.Selection.name.Contains("Lock_Button"))
				{
					StartCoroutine(ButterflyController.Instance.TapableLockBtn());
					StartCoroutine(WaitBarrier(2f,ParentsBarrier.BarrierTypes.BuyFullVersion));
				}
			}
		}

		public IEnumerator ReloadGame()
		{
			UILoadingScreen.SetActive(true);
			yield return new WaitForSeconds(1.5f);
			Application.LoadLevel(Application.loadedLevelName);
		}

		private IEnumerator WaitBarrier(float time, ParentsBarrier.BarrierTypes type)
		{
			yield return new WaitForSeconds(time);
			SoundsManager.Play("ForParents2");
			UIParentsBarrier.gameObject.GetComponent<ParentsBarrier>().GenerateSwipeDirection(type);
			UIParentsBarrier.transform.localScale = new Vector3(2f,2f,1f);
			UIParentsBarrier.SetActive(true);
			GameObject.Find("InputManager").GetComponent<InputManager>().enabled = false;
		}

		public void ShowUILabels(GameObject label, StartingPos initPos)
		{
			UIOpened = true;
			label.SetActive(true);
			switch(initPos)
			{
				case StartingPos.Top:
					label.transform.localPosition = VectorsCoordinatePositions[0];
					break;
				case StartingPos.Middle:
					label.transform.localPosition = VectorsCoordinatePositions[1];
					break;
				case StartingPos.Down:
					label.transform.localPosition = VectorsCoordinatePositions[2];
					break;
				default:
					label.transform.localPosition = Vector3.zero;
					break;
			}

			iTween.MoveTo(label,iTween.Hash("position", VectorsCoordinatePositions[1], "islocal", true, "time", 1));
		}

		public IEnumerator HideUILabels(GameObject label, StartingPos endPos)
		{
			Vector3 _endPosValue;
			switch(endPos)
			{
				case StartingPos.Top:
					_endPosValue = VectorsCoordinatePositions[0];
					break;
				case StartingPos.Middle:
					_endPosValue = VectorsCoordinatePositions[1];
					break;
				case StartingPos.Down:
					_endPosValue = VectorsCoordinatePositions[2];
					break;
				default:
					_endPosValue = Vector3.zero;
					break;
			}
			
			iTween.MoveTo(label,iTween.Hash("position", _endPosValue, "islocal", true, "time", 1));
			yield return new WaitForSeconds(1);
			label.SetActive(false);
			UIOpened = false;
		}

	}
}
