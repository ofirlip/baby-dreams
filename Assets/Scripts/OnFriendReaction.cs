﻿
namespace Assets.Scripts
{
    public enum OnFriendReaction
    {
        Happy1 = 0,
        Happy2 = 1,
        MeetFriend1 = 2,
        MeetFriend1Long = 3,
        MeetFriend2 = 4,
        OO = 5,
		ButterflyMeetSun = 6,
		ButterflyMeetBunnies = 7,
		ButterflyMeetTreeBirds = 8,
		ButterflyMeetCloudLove = 9,
		ButerflymeetDance = 10,
		ButerflymeetDog = 11,
		ButerflymeetClapHands = 12,
		ButerflymeetBees = 13,
		ButerflymeetFly = 14,
		ButerflymeetEatCarrot = 15,
		ButerflymeetEatApple = 16,
		ButerflymeetUpperFlowers = 17,
		ButerflymeetEatOO = 18,
		ButerflymeetIceCream = 19,
		ButerflymeetPlayGoodNightTune = 20,
    }
}
