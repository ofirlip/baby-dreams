﻿using System;
using UnityEngine;
using System.Collections.Specialized;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Xml;
using System.Linq;

public class Utils : MonoBehaviour 
{
	public static bool EqualVectors(Vector3 a, Vector3 b, float angleError)
	{
		if(!Mathf.Approximately(a.magnitude, b.magnitude))
			return false;
		
		float cosAngleError = Mathf.Cos(angleError * Mathf.Deg2Rad);     
		
		//A value between -1 and 1 corresponding to the angle.      
		float angle = Vector3.Dot(a.normalized, b.normalized);     
		//The dot product of normalized Vectors is equal to the cosine of the angle between them.
		//So the closer they are, the closer the value will be to 1.  
		// Opposite Vectors will be -1 and orthogonal Vectors will be 0.      
		if(angle >= cosAngleError) 
		{     
			//If angle is greater, that means that the angle between the two vectors is less than the error allowed.
			return true;     
		}     
		return false;
	}
	public static void ToggleParticles(Transform t)
	{
		t.GetComponent<ParticleEmitter>().emit = !t.GetComponent<ParticleEmitter>().emit;
	}
	public static void ToggleParticles(ParticleEmitter p)
	{
		p.emit = !p.emit;
	}
	public static bool ApproxEqualVectors(Vector3 a, Vector3 b, float threshold)
	{
		return (a.x > b.x - threshold && a.x < b.x + threshold &&
		        a.y > b.y - threshold && a.y < b.y + threshold);
	}
	
	public static bool ApproxEqualFloats(float a, float b, float maxGap)
	{
		return Mathf.Abs(a - b) < maxGap;
	}
	public static GameObject GetGameObjectFromList(GameObject obj, List<GameObject> list)
	{
		return list.Find( delegate(GameObject o){ return o == obj; });
	}
	public static bool IsObjInArray(GameObject obj, GameObject[] arr)
	{
		foreach(GameObject o in arr)
		{
			if (o == obj)
			{
				return true;
			}
		}
		return false;
	}
	public static bool IsObjInList(GameObject obj, List<GameObject> list)
	{
		return list.Find( delegate(GameObject o){ return o == obj; }) != null;
	}
	
	public static bool IsRendered(Transform t)
	{
		// by own renderer
		if (t.GetComponent<Renderer>() != null)
		{
			return t.GetComponent<Renderer>().isVisible;	
		}
		
		// by child renderers
		if (t.GetComponentsInChildren<Renderer>().Length > 0)
		{
			return ((Renderer)t.GetComponentsInChildren<Renderer>()[0]).isVisible;
		}
		
		// none found
		return false;
	}
	
	public static Transform GetRaycastHitTarget(Vector2 pos)
	{
		return GetRaycastHitTarget(new Vector3(pos.x, pos.y, 0f));
	}
	public static Transform GetRaycastHitTarget(Vector3 pos)
	{
		// Bit shift the index of the layer (8) to get a bit mask
		int bitMask = 1 << 8;
		// invert the bitmask to ignore layer 8 
		LayerMask layerMask = ~bitMask;
		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast(Camera.main.ScreenPointToRay(pos), out hit, 100f, layerMask)) 
		{
			//hit.transform.gameObject.SendMessage("OnMouseDown");
			return hit.transform;
		}
		return null;
	}
	public static void AssignPrefabToParent(Transform parent, string prefab, Vector3 offset)
	{
		GameObject child = null;
		try { child = parent.FindChild(prefab+"(Clone)").gameObject; } catch(System.Exception e) {}
		if (child == null)
		{
			child = (GameObject)Instantiate(Resources.Load(prefab));
			child.transform.parent = parent;
		}
		if (child != null)
		{
			child.transform.position = new Vector3(parent.position.x + offset.x, parent.position.y + offset.y, parent.position.z + offset.z);
		}
	}
	
	public static Hashtable Hash(params object[] args)
	{
		Hashtable hashTable = new Hashtable(args.Length/2);
		if (args.Length %2 != 0) {
			Debug.LogError("Tween Error: Hash requires an even number of arguments"); 
			return null;
		}else{
			int i = 0;
			while(i < args.Length - 1) {
				hashTable.Add(args[i], args[i+1]);
				i += 2;
			}
			return hashTable;
		}
	}	
	
	public static Dictionary<string, string> Dic(params string[] args)
	{
		Dictionary<string, string> dic = new Dictionary<string, string>(args.Length/2);
		if (args.Length %2 != 0) {
			Debug.LogError("Params Error: Odd number of arguments"); 
			return null;
		}else{
			int i = 0;
			while(i < args.Length - 1) {
				dic.Add(args[i], args[i+1]);
				i += 2;
			}
			return dic;
		}
	}
	
	public static Dictionary<string, XmlDocument> _xmlDocs = new Dictionary<string, XmlDocument>();
	public static XmlDocument GetXMLDocFromURL(string url)
	{
		return GetXMLDocFromURL(url, false);
	}
	public static XmlDocument GetXMLDocFromURL(string url, bool refreshCache)
	{
		if (!refreshCache && _xmlDocs.ContainsKey(url))
		{
			return _xmlDocs[url];
		}
		
		XmlDocument doc = new XmlDocument();
		string page = Web.GetPage(url);
		if (page != string.Empty)
		{
			doc.LoadXml(page);
			_xmlDocs.Add(url, doc);
		}
		return doc;
	}
	
	
	
	public class MoreAppsScreen : MonoBehaviour
	{
		private List<Texture2D> images = new List<Texture2D>();
		Transform _parent;
		
		public MoreAppsScreen(Transform parent)
		{
			_parent = parent;	
		}
		public void BuildScreen()
		{
			XmlDocument doc = new XmlDocument();
			doc.LoadXml(Web.GetPage("apps.xml"));
			foreach(XmlNode n in doc.SelectNodes("//app"))
			{
				Debug.Log(n.SelectSingleNode("name").InnerText);
				StartCoroutine("LoadImage2");
			}
		}
		IEnumerator LoadImage2()
		{
			Debug.Log("LoadImage2");
			yield return 0;
		}
		private IEnumerator LoadImage()
		{
			Debug.Log("LoadImage");
			WWW www = new WWW("dragon.jpg");
			yield return www;
			Texture2D t = new Texture2D(1024, 1024, TextureFormat.DXT1, false);
			www.LoadImageIntoTexture(t);
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.parent = _parent;
			cube.GetComponent<Renderer>().material.mainTexture = t;
		}	
	}
}

