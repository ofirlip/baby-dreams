﻿using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts
{
	public class TapableItem : MonoBehaviourBase
    {
        public Animator Animator;
        public string TapTrigger = "tap";
	    public float LookAtDuration = 2f;
		public Transform MagicPivot;
		public bool AlreadyClicked;

        public void PlayTapAnimation()
        {
            if (Animator == null)
                Debug.Log("Animator is null for object: " + name);
            else
                Animator.SetTrigger(TapTrigger);
        }
    }
}
