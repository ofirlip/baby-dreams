﻿using System;
using System.Collections;
using System.Linq;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.GameEvents;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts
{
    public class ButterflyController : CommandMonoBehaviour
    {
        private string _currentXDirection;
        private AnimatorEvents _animatorEvents;
        private bool _isMoving;
        private Action _movementFinishedCallback;
		private string _lastDirection;

        private Transform _friendLandingPivot;
		private FriendsGroup _friendsGroup;
        
		public Animator MoonSunAnimator;
        public Transform Butterfly;
        public Animator _animator;
        public Animator[] _elementsAnimators;
        public float MovementSpeed;
        public float HelloTime;
        public Vector3 Destination;

		public CameraController CameraController;
        public InputManager InputManager;

		public GameObject[] GamePlayObjectsToActivate;

		public Camera MainCamera;
		public Camera BedroomCamera;

		public float _nightStartX = 1.0f;
		private bool _nightStartXOnce = true;
		public float _nightStartArea = 1.0f;
		private bool _nightStartAreaOnce = true;
		public float _nightStartHome = 1.0f;
		private bool _nightStartHomeOnce = true;
		public bool _blockButterflyToCamera = false;

		public static bool _isDay = true;
		public bool	CollisionAnimations;
		public ButterflyCollisionDetect	ButterflyCollisionObj;
		public GameObject	UIMainMenu;

		public static ButterflyController Instance { get; private set; }

        public void Awake()
        {
			Instance = this;
			
            _animatorEvents = new AnimatorEvents(_animator);
            _currentXDirection = "Front";
            Subscribe(MetagameEvents.FriendSelected, _ => OnFriendSelected(_.Value));
			Subscribe(MetagameEvents.FriendFromGroupSelected, _ => OnFriendFromGroupSelected(_.Value));
            Subscribe(MetagameEvents.ButterflyDragStarted, _ => OnButterflyDragStarted());
            Subscribe(MetagameEvents.ButterflyDrag, _ => OnButterflyDrag(_.Value));
            Subscribe(MetagameEvents.ButterflyDragFinished, _ => OnButterflyDragFinished());
            Subscribe(MetagameEvents.MovingStateChanged, _ => OnGameStateChanged(_.Value));
            Subscribe(MetagameEvents.LookAreaSelected, _ => OnLookAreaSelected(_.Value));
            Subscribe(MetagameEvents.TapableItemSelected, _ => StartCoroutine(OnTapableItemSelected(_.Value)));
        }

		public void Start()
		{
			if(CollisionAnimations)
				ButterflyCollisionObj.CollisionDetectActive = true;
			else ButterflyCollisionObj.CollisionDetectActive = false;

			StopGamePlay();
		}

		public void StopGamePlay()
		{
			InputManager.enabled = false;	
			for(int i=0;i<GamePlayObjectsToActivate.Length;i++)
			{
				GamePlayObjectsToActivate[i].SetActive(false);
			}
		}

		public void LoadLevelNight()
		{
			Debug.Log ("LoadLevelNight");
			_nightStartHomeOnce = true;
			_nightStartAreaOnce = true;
			_nightStartXOnce = true;
			MoonSunAnimator.gameObject.SetActive(false);
			ButterflyController._isDay = true;
			MetagameEvents.ScreenChanged.Publish(new GameEventArgs<int>(7));
			ActivateGamePlayScene();

			_animator.gameObject.transform.position = new Vector3(_nightStartX,4.4f,-5f);
			MainCamera.transform.position = new Vector3(_nightStartX+2,4.5f,-10f);
			MainCamera.depth = 1;
			BedroomCamera.depth = 0;
			_blockButterflyToCamera = false;	
			SoundsManager.FadeInNightSound();
			StartCoroutine(DisableMenu());
		}

		IEnumerator DisableMenu()
		{
			yield return new WaitForSeconds(1);
			UIMainMenu.SetActive(false);
//			UIBedroom.SetActive(false);
		}

		public void LoadLevelDay()
		{
			Debug.Log ("LoadLevelDay");
			_nightStartHomeOnce = true;
			_nightStartAreaOnce = true;
			_nightStartXOnce = true;
			MoonSunAnimator.gameObject.SetActive(false);
			ButterflyController._isDay = true;

			MetagameEvents.ScreenChanged.Publish(new GameEventArgs<int>(1));
			ActivateGamePlayScene();

			_animator.gameObject.transform.position = new Vector3(16.7f,4.4f,-5f);
			MainCamera.transform.position = new Vector3(-17.5f,4.5f,-10f);
			MainCamera.depth = 1;
			BedroomCamera.depth = 0;
			GameObject.Find("HouseManager").GetComponent<HouseManager>().StartSceneSettings();
			SoundsManager.FadeInDaySound();
			_blockButterflyToCamera = false;
			StartCoroutine(DisableMenu());
		}

		private void ActivateGamePlayScene()
		{
			for(int i=0;i<GamePlayObjectsToActivate.Length;i++)
			{
				GamePlayObjectsToActivate[i].SetActive(true);
			}
		}

        #region Drag and Drop

        private void OnButterflyDragStarted()
        {
            GameStateManager.MovingState = MovingState.Flying;
        }

        private void OnButterflyDrag(Vector2 position)
        {
			OnPositionSelected(position);
        }

        private void OnButterflyDragFinished()
        {
            GameStateManager.MovingState = MovingState.Idle;
        }

        #endregion

        #region Moving

		IEnumerator ChangeSoundsFromDayToNight()
		{
			SoundsManager.FadeOutSound();
			yield return new WaitForSeconds(2.5f);
			SoundsManager.FadeInNightSound();
		}

		public override void Update()
		{
			base.Update();
			if (_isDay && Butterfly.transform.position.x >= 350 )
			{
				Debug.Log("----------------- Day to Night ------------------ ");

				if(_nightStartXOnce && Butterfly.transform.position.x <= 380)
				{
					MoonSunAnimator.gameObject.SetActive(true);
//					MoonSunAnimator.enabled = true;
					MoonSunAnimator.SetTrigger("DayToNightSunMoon");
					SoundsManager.Play("22DayComesToAnEnd");
					StartCoroutine(ChangeSoundsFromDayToNight());
					_nightStartXOnce = false;
				}
				_isDay = false;
				
			}
			else if (_nightStartAreaOnce && Butterfly.transform.position.x >= _nightStartArea)
			{
				SoundsManager.Play("23LetsFriendSleep");
				_nightStartAreaOnce = false;
			}
			else if(_nightStartHomeOnce && Butterfly.transform.position.x >= _nightStartHome)
			{
				SoundsManager.Play("25LetsGoBackHome");
				_nightStartHomeOnce = false;

			}
//			else if (!_isDay && Butterfly.transform.position.x < 350)
//			{
//				_isDay = true;
//			}

			if(_isMoving)
			{
				Vector2 destination = _friendLandingPivot != null ? _friendLandingPivot.position : Destination;				
				float distance = Vector2.Distance(destination, Butterfly.position);
				float speed = Mathf.Lerp(MovementSpeed/5f, MovementSpeed, distance/30f);				
				Vector2 translation = ((destination - Butterfly.Position2()).normalized)*speed*Time.deltaTime;
			    if (translation.magnitude <= distance && distance > 0.1f)
			    {
			        Butterfly.Translate(translation);
			    }
			    else
			    {
					Butterfly.position = Vector3.Slerp(Butterfly.position, new Vector3(destination.x, destination.y, Butterfly.position.z), 1.0f);
			        if (!InputManager.IsPressAndHold)
					{
			            StopFly();
					}
			    }

				if(MainCamera.transform.position.x>897 && !_blockButterflyToCamera)
				{
					GameObject.Find("HouseManager").GetComponent<HouseManager>().LockCameraOnNightHouse();
					_blockButterflyToCamera = true;
				}
			}
			else if (_friendLandingPivot != null)
			{
				Butterfly.SetXY(_friendLandingPivot.position.x, _friendLandingPivot.position.y);
			}

			if(_blockButterflyToCamera)
			{
				Butterfly.transform.position = new Vector3(Mathf.Clamp(Butterfly.transform.position.x,MainCamera.transform.position.x-12, MainCamera.transform.position.x+12),
				                                           Mathf.Clamp(Butterfly.transform.position.y,MainCamera.transform.position.y-8, MainCamera.transform.position.y+8),
				                                           Butterfly.transform.position.z);
			}
		}

        public void OnPositionSelected(Vector2 position)
        {
            if (GameStateManager.MovingState == MovingState.FriendMeeting)
			{
                return;
			}
            if (!_isMoving)
            {
                GameStateManager.MovingState = MovingState.Flying;
                _movementFinishedCallback = () =>
                {
					Debug.Log(position + " " + GameObject.Find("HouseManager").GetComponent<HouseManager>().NightButterflyHouseEnterPosition);
					if(new Vector2(Butterfly.position.x,Butterfly.position.y) == GameObject.Find("HouseManager").GetComponent<HouseManager>().NightButterflyHouseEnterPosition)
					{
						StartCoroutine(GameObject.Find("HouseManager").GetComponent<HouseManager>().NightGetInHouse());

					}
                    _animator.SetTrigger("StopFly");
                    _currentXDirection = "Front";
                    GameStateManager.MovingState = MovingState.Idle;

                };
            }
            MoveTo(position);
        }

        private void MoveTo(Vector2 position)
		{
			Destination = position;
			string direction = GetDirection(Butterfly.position, Destination);
			if(_isMoving)
			{
				if(direction != _lastDirection)
				{
                    ClearDirectionTriggers();
					_animator.SetTrigger("ChangeDirection");
					_animator.SetTrigger(direction);
				}
			}
			else
			{
                ClearDirectionTriggers();
				_animator.SetTrigger("StartFly");
				_animator.SetTrigger(direction);
				_isMoving = true;
			}
			_lastDirection = direction;
		}

		private void StopFly()
		{
			_isMoving = false;
			_lastDirection = "None";
			if(_movementFinishedCallback != null)
			{
				_movementFinishedCallback();
				_movementFinishedCallback = null;
			}
		}

        private void ClearDirectionTriggers()
        {
            _animator.ResetTrigger("Right");
            _animator.ResetTrigger("RightDown");
            _animator.ResetTrigger("Down");
            _animator.ResetTrigger("LeftDown");
            _animator.ResetTrigger("RightUp");
            _animator.ResetTrigger("Up");
            _animator.ResetTrigger("LeftUp");
            _animator.ResetTrigger("Left");
        }

        private string GetDirection(Vector2 currentPosition, Vector2 destination)
        {
            float angle = (destination - currentPosition).Angle();

            string direction;
            if (angle >= -10 && angle <= 10)
			{
                direction = "Right";
			}
            else if (angle >= -80 && angle <= -10)
			{
                direction = "RightDown";
			}
            else if (angle >= -100 && angle <= -80)
			{
                direction = "Down";
			}
            else if (angle >= -170 && angle <= -100)
			{
                direction = "LeftDown";
			}
            else if (angle >= 10 && angle <= 80)
			{
                direction = "RightUp";
			}
            else if (angle >= 80 && angle <= 100)
			{
                direction = "Up";
			}
            else if (angle >= 100 && angle <= 170)
			{
                direction = "LeftUp";
			}
            else
			{
                direction = "Left";
			}

            if (direction.StartsWith("Left"))
			{
                _currentXDirection = "Left";
			}
            else if (direction.StartsWith("Right"))
			{
                _currentXDirection = "Right";
			}
            else
			{
                _currentXDirection = "Front";
			}

            return direction;
        }

        #endregion

        #region Say Hello

        private void OnGameStateChanged(MovingState movingState)
        {
			if (movingState == MovingState.Idle && InputManager.enabled)
			{
                Invoke(() => SayHello(), HelloTime);
			}
            else
			{
                CancelInvoke(() => SayHello());
			}

            if (movingState == MovingState.Flying || movingState == MovingState.FriendMeeting)
            {
                StopCoroutine(LookLeft());
                StopCoroutine(LookRight());
            }
        }

        private void SayHello()
        {
            _animator.SetTrigger("SayHello");
            Invoke(() => SayHello(), HelloTime);
        }

        #endregion

        #region Look left and right

        private void OnLookAreaSelected(Direction direction)
        {
			// disable this func
			return;

            if (GameStateManager.MovingState == MovingState.Idle ||
                GameStateManager.MovingState == MovingState.LookingLeft ||
                GameStateManager.MovingState == MovingState.LookingRight)
            {
                if (direction == Direction.Left)
                {
                    if (GameStateManager.MovingState != MovingState.LookingLeft)
					{
                        StartCoroutine(LookLeft());
					}
                }
                else
                {
                    if (GameStateManager.MovingState != MovingState.LookingRight)
					{
                        StartCoroutine(LookRight());
					}
                }
            }
        }

        private IEnumerator LookLeft()
        {
			// disable this func
			yield return false;

            StopCoroutine(LookRight());
            GameStateManager.MovingState = MovingState.LookingLeft;
            _animator.SetTrigger("LookLeft");
            yield return StartCoroutine(_animatorEvents.WaitExit("LookLeft"));
            GameStateManager.MovingState = MovingState.Idle;
        }

        private IEnumerator LookRight()
        {
			// disable this func
			yield return false;
			
            StopCoroutine(LookLeft());
            GameStateManager.MovingState = MovingState.LookingRight;
            _animator.SetTrigger("LookRight");
            yield return StartCoroutine(_animatorEvents.WaitExit("LookRight"));
            GameStateManager.MovingState = MovingState.Idle;
        }

        #endregion

        #region Friend meeting

        private MeetingSettings _meetingSettings;

        public void OnFriendFromGroupSelected(FriendFromGroup friendFromGroup)
		{
			if (GameStateManager.MovingState == MovingState.FriendMeeting)
			{
				if(friendFromGroup.Group == _friendsGroup && !friendFromGroup.IsActivated)
				{
                    StopAllCoroutines();
                    StartCoroutine(ActivateMoreFriend(friendFromGroup));
				}
			}
			else
			{
				GameStateManager.MovingState = MovingState.FriendMeeting;
				//&& (PlayerPrefs.GetString("AlreadyBoughtGame","No") == "Yes" || Destination.x<130 || (Destination.x>470 && Destination.x<680) || Destination.x>860 )
				if(!friendFromGroup.Group.AlreadyClicked )
				{
					_friendsGroup = friendFromGroup.Group;
					_friendsGroup.AlreadyClicked = true;
					friendFromGroup.IsActivated = true;
				    _meetingSettings = friendFromGroup.Group.GetMeetingSettings();
					FlyToGroupMagicPivot(friendFromGroup);
				}
				else 
				{
					_friendsGroup = friendFromGroup.Group;
					friendFromGroup.IsActivated = true;
					_meetingSettings = friendFromGroup.Group.GetMeetingSettings();
					PlayDistanceMeetingRoutine(friendFromGroup);
				}
			}
		}

        public void OnFriendSelected(Friend friend)
        {
			if (GameStateManager.MovingState != MovingState.FriendMeeting)
			{
				Debug.Log("friend clicked");
				GameStateManager.MovingState = MovingState.FriendMeeting;
			    _meetingSettings = friend.GetMeetingSettings();
				// && (PlayerPrefs.GetString("AlreadyBoughtGame","No") == "Yes" || Destination.x<130 || (Destination.x>470 && Destination.x<680) || Destination.x>860 )
				if(!friend.AlreadyClicked)
				{
					friend.AlreadyClicked = true;
					FlyToMagicPivot(friend);
				}
				else 
				{
					Debug.Log("Already clicked this!");
					PlayDistanceMeetingRoutine(friend);
				}
			}
		}

		private IEnumerator PlayMeetingRoutine(TapableItem tapableItem)
		{
			GameStateManager.MovingState = MovingState.FriendMeeting;

			_movementFinishedCallback = null;
			StopFly();

			_animator.SetTrigger("StartMagic");
			_animator.SetTrigger(_currentXDirection);
			yield return StartCoroutine(_animatorEvents.WaitExit("Magic"));
			FlyToLandingPivot(tapableItem);
		}

		private void PlayDistanceMeetingRoutine(TapableItem tapableItem)
		{
			GameStateManager.MovingState = MovingState.FriendMeeting;
			
			_movementFinishedCallback = null;
			StopFly();
			StartCoroutine(SecondTapFriend(tapableItem));
		}

		private void FlyToGroupMagicPivot(FriendFromGroup tapableItem)
		{
			Transform _friendMagicPivot = tapableItem.MagicPivot;
			MoveTo(_friendMagicPivot.position);
			_movementFinishedCallback = () => OnFlyToMagicPivotFinished(tapableItem);
		}

		private void FlyToMagicPivot(TapableItem tapableItem)
		{
			Transform _friendMagicPivot = tapableItem.MagicPivot;
			MoveTo(_friendMagicPivot.position);
			_movementFinishedCallback = () => OnFlyToMagicPivotFinished(tapableItem);
		}

        private void FlyToLandingPivot(TapableItem tapableItem)
        {
            _friendLandingPivot = _meetingSettings.LandingPivot;
            MoveTo(_friendLandingPivot.position);
			_movementFinishedCallback = () => OnFlyToLandingPivotFinished(tapableItem);
            CameraController.MoveToFriendCameraPivot(_meetingSettings.CameraPivot);
            CameraController.FocusCameraOnFriend(_meetingSettings.CameraFocusSize);
        }

		private void OnFlyToMagicPivotFinished(TapableItem tapableItem)
		{
			StartCoroutine(PlayMeetingRoutine(tapableItem));
		}

		private IEnumerator SecondTapFriend(TapableItem tapableItem)
		{
			_animator.SetTrigger("StopFly");
			_currentXDirection = "Front";

			tapableItem.PlayTapAnimation();
			_animator.SetInteger("FriendReaction", (int) _meetingSettings.ButterflyReaction);
			
			if (_friendsGroup != null)
			{
				List<FriendFromGroup> notActivatedFriends = _friendsGroup.Friends.Where(_ => !_.IsActivated).ToList();
				if (notActivatedFriends.Any())
				{
					FriendFromGroup friend = GameRandom.NextItem(notActivatedFriends);
					friend.IsActivated = true;
					StartCoroutine(ActivateFriend(friend));
					yield break;
				}
			}
			FinishFriendMeeting();
		}

		private void OnFlyToLandingPivotFinished(TapableItem tapableItem)
		{
			_animator.SetTrigger("StopFly");
			_currentXDirection = "Front";
            StartCoroutine(ActivateFriend(tapableItem));
		}

        private IEnumerator ActivateFriend(TapableItem tapableItem)
        {
            tapableItem.PlayTapAnimation();
            _animator.SetTrigger("LookAtFriend");
            yield return StartCoroutine(_animatorEvents.WaitEnter("LookAtFriend"));
            _animator.SetTrigger(_meetingSettings.ButterflyLookAt.ToString());

            yield return new WaitForSeconds(tapableItem.LookAtDuration);
            _animator.SetTrigger("StopLookAtFriend");
			_animator.SetInteger("FriendReaction", (int) _meetingSettings.ButterflyReaction);

			if (_friendsGroup != null)
			{ 
				yield return new WaitForSeconds(1);
				List<FriendFromGroup> notActivatedFriends = _friendsGroup.Friends.Where(_ => !_.IsActivated).ToList();
				StartCoroutine(ActivateGroupFriends(notActivatedFriends));
			}
			else
			{
				yield return StartCoroutine(_animatorEvents.WaitExit("ReactToFriend"));
				FinishFriendMeeting();
			}
        }

		private IEnumerator ActivateGroupFriends(List<FriendFromGroup> friendFromGroup)
		{
			for(int i = 0 ;i<friendFromGroup.Count;i++)
			{
				FriendFromGroup friend = friendFromGroup[i];
				yield return new WaitForSeconds(friend.FriendsActivationTimer);

				friend.IsActivated = true;
				friend.PlayTapAnimation();
			}

			FinishFriendMeeting();
		}

        private IEnumerator ActivateMoreFriend(FriendFromGroup friendFromGroup)
        {
            _animator.SetTrigger("StartMagic");
            yield return StartCoroutine(_animatorEvents.WaitExit("Magic"));
            friendFromGroup.IsActivated = true;
            StartCoroutine(ActivateFriend(friendFromGroup));
        }

        private void FinishFriendMeeting()
        {
            _friendLandingPivot = null;
            _friendsGroup = null;
            _meetingSettings = null;
            CameraController.UnFocusCamera();
            GameStateManager.MovingState = MovingState.Idle;
        }

        #endregion

        public IEnumerator OnTapableItemSelected(TapableItem tapableItem)
        {
			Debug.Log("tapable clicked");
            if (GameStateManager.MovingState == MovingState.FriendMeeting)
			{
                yield break;
			}
			GameStateManager.MovingState = MovingState.FriendMeeting;
			
			if(!tapableItem.AlreadyClicked)
			{
				tapableItem.AlreadyClicked = true;
				FlyToMagicPivotTapableItem(tapableItem);
			}
			else 
			{
				Debug.Log("Second Tap this tappable!");
				SecondTapTapableItemMeeting(tapableItem);
			}
        }

		private void FlyToMagicPivotTapableItem(TapableItem tapableItem)
		{
			Transform _friendMagicPivot = tapableItem.MagicPivot;
			MoveTo(_friendMagicPivot.position);
			_movementFinishedCallback = () => FlyToMagicPivotTapableItemFinished(tapableItem);
		}

		private void FlyToMagicPivotTapableItemFinished(TapableItem tapableItem)
		{
			StartCoroutine(TapableItemMeeting(tapableItem));
		}

		private void SecondTapTapableItemMeeting(TapableItem tapableItem)
		{
			tapableItem.PlayTapAnimation();
			GameStateManager.MovingState = MovingState.Idle;
		}


		private IEnumerator TapableItemMeeting(TapableItem tapableItem)
		{
			tapableItem.PlayTapAnimation();
			_animator.SetTrigger("StartLesserMagic");
			_animator.SetTrigger(_currentXDirection);
			yield return StartCoroutine(_animatorEvents.WaitExit("LesserMagic"));
			GameStateManager.MovingState = MovingState.Idle;
		}

		public IEnumerator TapableLockBtn()
		{
			Debug.Log("Lock btn pressed");
			GameStateManager.MovingState = MovingState.Idle;
			_animator.SetTrigger("StartLesserMagic");
			yield return StartCoroutine(_animatorEvents.WaitExit("LesserMagic"));
		}

    }

	public class MeetingSettings
	{
		public Transform LandingPivot;
		public Transform CameraPivot;
		public float CameraFocusSize;
		public Direction ButterflyLookAt;
		public OnFriendReaction ButterflyReaction;
		public bool AlreadyClicked;
	}
}
