﻿using System;
using System.Collections;
using System.Linq;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.GameEvents;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts
{
	public class ParentsBarrier : CommandMonoBehaviour
	{
	    public GameObject[] barrierText;
	    private int swipeDirection; //0=left, 1=right; 2=up, 3=down
	    private string swipeLabel;
		private bool stickToMainCamera;

		public enum BarrierTypes
		{
			SocialSharing,
			OptionsScreen,
			MoreApps,
			ParentsWindow,
			BuyFullVersion
		}
		private BarrierTypes _barrier;


		void Update()
		{
			if(stickToMainCamera)
				this.transform.position = new Vector3(GameObject.Find("MainCamera").transform.position.x,
				                                       GameObject.Find("MainCamera").transform.position.y,
				                                      	this.transform.position.z);
		}

		public void GenerateSwipeDirection(BarrierTypes barrier)
	    {
			_barrier = barrier;
	        swipeDirection = UnityEngine.Random.Range(0, 4);
	        for (int i = 0; i < barrierText.Length; i++)
			{
	            if (i == swipeDirection)
					barrierText[i].SetActive(true);
				else
					barrierText[i].SetActive(false);
			}

			if(_barrier == BarrierTypes.BuyFullVersion)
			{
				this.transform.position = new Vector3(GameObject.Find("MainCamera").transform.position.x,
				                                      GameObject.Find("MainCamera").transform.position.y,
				                                      this.transform.position.z);
				stickToMainCamera = true;
			}
	    }

		void OnDisable()
		{
			stickToMainCamera = false;
		}

	    public void OnSwipe(SwipeGesture gesture)
	    {
	        float velocity = gesture.Velocity;
	        FingerGestures.SwipeDirection direction = gesture.Direction;
	        float vel;
	#if UNITY_ANDROID
			vel=200f;
	#else
	        vel = 200;
	#endif

	        if (velocity > vel)
	            if (((direction == FingerGestures.SwipeDirection.Left) && (swipeDirection == 0))
	                ||
	                ((direction == FingerGestures.SwipeDirection.Right) && (swipeDirection == 1))
	                ||
	                ((direction == FingerGestures.SwipeDirection.Up) && (swipeDirection == 2))
	                ||
	                ((direction == FingerGestures.SwipeDirection.Down) && (swipeDirection == 3)))
	            {
	                barrierComplete();
	            }
	    }

	    public void OnTap(TapGesture gesture)
	    {
			Debug.Log("a " + gesture.Selection);
	        GameObject _tappedObject = gesture.Selection;
	        if (gesture.Selection != null)
	        {
				Debug.Log(_tappedObject.name);
	            if ((_tappedObject.name == "cancelParent") || (_tappedObject.name == "Close_BTN"))
	            {
	                hideBarrier();
	            }
	        }
	    }

	    private void hideBarrier()
		{	
//			StartCoroutine(GameObject.Find("UIManager").GetComponent<UIManager>().HideUILabels(this.gameObject,UIManager.StartingPos.Down));

	        switch (swipeDirection)
	        {
	            case 0:
	                swipeLabel = "left";
	                break;
	            case 1:
	                swipeLabel = "right";
	                break;
	            case 2:
	                swipeLabel = "up";
	                break;
	            case 3:
	                swipeLabel = "down";
	                break;
	        }

	#if UNITY_IPHONE
	        //FlurryAnalytics.logEventWithParameters("FailedBarrier", Utils.Dic("Direction", swipeLabel), false);
	#endif

	        for (int i = 0; i < barrierText.Length; i++)
			{
	            barrierText[i].SetActive(false);
			}

	        this.gameObject.SetActive(false);
		  }

	    private void barrierComplete()
	    {
	        if (_barrier == BarrierTypes.OptionsScreen)
	        {
	        }
	        else if (_barrier == BarrierTypes.SocialSharing)
	        {
	        }
			else if (_barrier == BarrierTypes.MoreApps)
			{
				Application.OpenURL("https://itunes.apple.com/us/app/baby-animals/id899588877?ls=1&mt=8");
			}
			else if(_barrier == BarrierTypes.ParentsWindow)
			{
				GameObject.Find("MainMenuCamera").transform.position = new Vector3(-98.94f,-2.5f,-10f);
				GameObject.Find("UIManager").GetComponent<UIManager>().ParentsWindow.SetActive(true);
				GameObject.Find("UIManager").GetComponent<UIManager>().HomeWindow.SetActive(false);
			}
			else if(_barrier == BarrierTypes.BuyFullVersion)
			{
				this.transform.localScale = new Vector3(1f,1f,1f);
				GameObject.Find("InputManager").GetComponent<InputManager>().enabled = true;
			}
	        hideBarrier();
			Debug.Log("Barrier Completed");
	    }
	}
}
