﻿using System;
using System.Collections;
using System.Linq;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.GameEvents;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts
{
	public class HouseManager : CommandMonoBehaviour {

		public Animator 		HouseButterflyAnimator;
		public Animator			NightHouseButterflyAnimator;
		public GameObject		ButterflyHouse;
		public GameObject		ButterflyPlayer;
		public GameObject		HintObj;
		public InputManager		InputManagerObj;

		public CameraController	CameraController;
		public ButterflyController	ButterflyControllerObj;
		public BedRoomButterflyManager	BedroomManager;

		private Vector3			hintStartingHousePos = new Vector3(-19f,-4f,0);
		private Vector3			houseHintPos = new Vector3(-19f,5f,0);
		private Vector3			hintStartingNightHousePos = new Vector3(896.5f,-4f,0);
		private Vector3			nightHouseHintPos = new Vector3(896.5f,2f,0);


		private AnimatorEvents 	houseButterFlyAnimatorEvents;
		private bool			houseTimerStart;
		private float			houseTimer;
		private bool			houseTapped;
		private bool			nightHouseTapped;
		private bool			nightButterflyHouseEnterPositionReached;
		public Vector2			NightButterflyHouseEnterPosition;
		public UIManager		UiManager;



		void Awake()
		{
			houseButterFlyAnimatorEvents = new AnimatorEvents(HouseButterflyAnimator);
		}

		void Start()
		{
			StartSceneSettings();
			houseTimerStart = false;
			HouseButterflyAnimator.enabled = false;
		}

		void SoundsManagerPlaySound( string clipName)
		{
			SoundsManager.Play(clipName);
		}
		IEnumerator SoundsManagerPlaySoundWait(float timer, string clipName)
		{
			yield return new WaitForSeconds(timer);
			SoundsManager.Play(clipName);
		}

		void Update()
		{
			if(houseTimerStart)
			{
//				Debug.Log(houseTimer + " " + CameraController.Butterfly);
				houseTimer+=Time.deltaTime;
				if(houseTimer>5 && CameraController.Butterfly == ButterflyHouse.transform)
					StartCoroutine(HouseHint());
				else if(houseTimer>5 && CameraController.Butterfly == NightHouseButterflyAnimator.transform)
					StartCoroutine(NightHouseHint());
			}
//			if(ButterflyControllerObj.Destination.x == NightButterflyHouseEnterPosition.x && !nightButterflyHouseEnterPositionReached)
//			{
//				nightButterflyHouseEnterPositionReached = true;
//				StartCoroutine(NightGetInHouse());
//			}
		}

		public void OnTap(TapGesture gesture)
		{
			if(gesture.Selection != null && gesture.Selection.name == "HouseCollider" && houseTimer>2 && !houseTapped)
			{
				houseTapped = true;
				StartCoroutine(GetOutOfHouse());
			}
			else if(gesture.Selection != null && gesture.Selection.name == "HouseColliderNight" && !nightHouseTapped)
			{
				Debug.Log("Night");
				LockCameraOnNightHouse();
				nightHouseTapped = true;
				ButterflyControllerObj.OnPositionSelected(NightButterflyHouseEnterPosition);
				InputManagerObj.enabled = false;
			}
		}

		public void StartSceneSettings()
		{
			nightHouseTapped = false;
			houseTimer = 0;
			houseTapped = false;
			houseTimerStart = true;
			CameraController.Butterfly = ButterflyHouse.transform;
			InputManagerObj.enabled = false;
			HouseButterflyAnimator.enabled = true;
			HouseButterflyAnimator.SetTrigger("Enter");
			ButterflyPlayer.SetActive(true);
			ButterflyHouse.SetActive(true);
		}

		public IEnumerator NightGetInHouse()
		{
			houseTimerStart = false;
			houseTimer = 0;
			yield return new WaitForSeconds(1.2f);

			NightHouseButterflyAnimator.SetTrigger("tap");
			SoundsManager.FadeOutSound();

			ButterflyControllerObj._blockButterflyToCamera = false;
			ButterflyPlayer.transform.position = new Vector3(1000,1000,0);
			yield return new WaitForSeconds(2.5f);

			PlayerPrefs.SetString("GetToBedroom","Yes");
			StartCoroutine(UiManager.ReloadGame());
//			StartCoroutine(BedroomManager.ReturnToBedroom());
//			ButterflyPlayer.gameObject.SetActive(true);

		}


		public void LockCameraOnNightHouse()
		{
			CameraController.Butterfly = NightHouseButterflyAnimator.transform;
			houseTimer = 0;
			houseTimerStart = true;
		}

	
		private IEnumerator GetOutOfHouse()
		{
			houseTimerStart = false;
			HintObj.SetActive(false);

			HouseButterflyAnimator.SetTrigger("ButterflyGetsOutOfTheHouse");
			yield return StartCoroutine(houseButterFlyAnimatorEvents.WaitExit("ButterflyGetsOutOfTheHouse"));
			ButterflyPlayer.transform.position = ButterflyHouse.transform.position;

			StartCoroutine(SoundsManagerPlaySoundWait(0.7f,"17HelloLittleFriend"));
			StartCoroutine(SoundsManagerPlaySoundWait(2.7f,"18WakeUp"));
			ActivateGame();
		}

		public void ActivateGame()
		{
			ButterflyHouse.SetActive(false);
			CameraController.Butterfly = ButterflyPlayer.transform;
			InputManagerObj.enabled = true;
			ButterflyControllerObj.Destination.x = 0;
			nightButterflyHouseEnterPositionReached = false;
		}

		private IEnumerator HouseHint()
		{
			HintObj.SetActive(true);
			HintObj.transform.localPosition = hintStartingHousePos;
			iTween.MoveTo(HintObj,iTween.Hash("position", houseHintPos, "islocal", true, "time", 1f));
			if(houseTimerStart)
				SoundsManagerPlaySound("16TapTheHouse");
			houseTimerStart = false;
			yield return new WaitForSeconds(4);
			iTween.MoveTo(HintObj,iTween.Hash("position", hintStartingHousePos, "islocal", true, "time", 1f));
		}

		private IEnumerator NightHouseHint()
		{
			HintObj.SetActive(true);
			HintObj.transform.localPosition = hintStartingNightHousePos;
			iTween.MoveTo(HintObj,iTween.Hash("position", nightHouseHintPos, "islocal", true, "time", 1f));
			if(houseTimerStart)
				SoundsManagerPlaySound("16TapTheHouse");
			houseTimerStart = false;
			yield return new WaitForSeconds(4);
			iTween.MoveTo(HintObj,iTween.Hash("position", hintStartingNightHousePos, "islocal", true, "time", 1f));
		}
	}
}
