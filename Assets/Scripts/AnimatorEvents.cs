﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts
{
    public class AnimatorEvents
    {
        private readonly StateEvents[] _behaviors;

        public AnimatorEvents(Animator animator)
        {
            _behaviors = animator.GetBehaviours<StateEvents>();
        }

        public IEnumerator WaitExit(string name)
        {
//			Debug.Log ("WaitExit "+_behaviors.Single(_ => _.Name == name).name);
            return _behaviors.Single(_ => _.Name == name).WaitExit();
        }

        public IEnumerator WaitEnter(string name)
        {
            return _behaviors.Single(_ => _.Name == name).WaitEnter();
        }
    }
}
