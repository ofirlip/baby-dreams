﻿using Assets.Scripts.Utils;
using Assets.Scripts.Utils.GameEvents;
using UnityEngine;

namespace Assets.Scripts
{
    public class ScreenBorder : MonoBehaviourBase
    {
        public int ScreenNumber;


        public void OnDrawGizmos()
        {
            Gizmos.DrawLine(
                new Vector2(Transform.position.x, Transform.position.y - 50f),
                new Vector2(Transform.position.x, Transform.position.y + 50f));
        }

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "ScreenSwitcher")
			{
				MetagameEvents.ScreenChanged.Publish(new GameEventArgs<int>(ScreenNumber + 1));
			}
        }

    }
}
