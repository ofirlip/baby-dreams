﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Utils;

namespace Assets.Scripts
{
	public class AnimationSound : MonoBehaviour
	{
		public void Play(string soundName)
		{
			SoundsManager.Play(soundName);
		}
		/*
		public void Play2(SoundEventTriggers.SoundTriggers trigger)
		{
			SoundsManager.Play2(trigger);
		}
		*/
	}
}
