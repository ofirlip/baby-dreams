﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Utils.GameEvents;

namespace Assets.Scripts
{
	public class HomePageButtons : MonoBehaviour 
	{
		public Animator		ButterflyAnimator;
		private bool		MenuSelected;
		
		void Start () 
		{
			MenuSelected = false;
		}

		public void OnTap(TapGesture gesture)
		{
			//GameObject.FindGameObjectWithTag("ButterflyController").GetComponent<ButterflyController>().LoadLevelDay();
			if(!MenuSelected)
			{
				GameObject _tappedObject = gesture.Selection;
				if (gesture.Selection != null)
				{
					Debug.Log(_tappedObject.name);
					if ((_tappedObject.name == "SunCollider"))
					{
						ButterflyAnimator.SetTrigger("SunSelected");
					}
					else if((_tappedObject.name == "MoonCollider"))
					{
						ButterflyAnimator.SetTrigger("MoonSelected");
					}
					MenuSelected = true;
				}
			}
		}
		
	}
}