﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SoundsManager : MonoBehaviour 
{
	private static SoundsManager _instance;
	
	public AudioClip[] Sounds;
	public AudioClip[] NarratorEnglish;
	public AudioClip[] BgSounds;
	public AudioSource AudioSource;
	public AudioSource AudioSourceBGs;
	enum audioInstance{Hold,FadeIn,FadeOut}

	private audioInstance audioCurrentInstance;


	
	public static SoundsManager Instance 
	{
		get { return _instance; } 
	}
	public void Awake()
	{
		_instance = this;
	}
	public void Start()
	{
		_instance.AudioSourceBGs.volume = 0;
		// testing
		//Play2(SoundEventTriggers.SoundTriggers.butterflyRandomHey);
	}

	void Update()
	{
		if(audioCurrentInstance == audioInstance.FadeIn)
		{
			_instance.AudioSourceBGs.volume += 0.1f*Time.deltaTime;
		}
		else if(audioCurrentInstance == audioInstance.FadeOut)
		{
			_instance.AudioSourceBGs.volume -= 0.1f*Time.deltaTime;
		}
		_instance.AudioSourceBGs.volume = Mathf.Clamp(_instance.AudioSourceBGs.volume,0f,0.2f);
	}
	public static void FadeInDaySound()
	{
		_instance.AudioSourceBGs.clip = _instance.BgSounds[0];
//		_instance.AudioSourceBGs.volume = 0.2f;
		if(PlayerPrefs.GetString("BgSoundMuted","No") == "No")
			_instance.audioCurrentInstance = audioInstance.FadeIn;

		_instance.AudioSourceBGs.Play();
	}

	public static void FadeOutSound()
	{
//		_instance.AudioSourceBGs.volume = 0f;
		_instance.audioCurrentInstance = audioInstance.FadeOut;
	}

	public static void FadeInNightSound()
	{
		_instance.AudioSourceBGs.clip = _instance.BgSounds[1];
//		_instance.AudioSourceBGs.volume = 0.2f;
		if(PlayerPrefs.GetString("BgSoundMuted","No") == "No")
			_instance.audioCurrentInstance = audioInstance.FadeIn;

		_instance.AudioSourceBGs.Play();
	}

	
	public static void Play(string soundName)
	{
//		Debug.Log ("Play("+soundName+")");
		_instance.PlayInternal(soundName);
	}

	private void PlayInternal(string soundName)
	{
		AudioClip clip = Sounds.FirstOrDefault(_ => _.name == soundName);
		if(clip == null)
		{
			Debug.Log("Warning! Can't find sound with name: " + soundName + ", checking the narrator list");
			clip = NarratorEnglish.FirstOrDefault(_ => _.name == soundName);

			if(clip == null)
			{
				//Debug.Log("Warning! "+ soundName + " can't be found on narrator list eighter.");
			}
			else 
			{
				AudioSource.PlayOneShot(clip);
			}
		}
		else
		{
			AudioSource.PlayOneShot(clip);
		}
	}
	
	#region Play 2
	/*
	public static void Play2(SoundEventTriggers.SoundTriggers trigger)
	{
		//Debug.Log ("PlayScript("+trigger.ToString()+")");
		_instance.PlayInternal2(trigger);
	}
	
	private void PlayInternal2(SoundEventTriggers.SoundTriggers trigger)
	{
		SoundEvent soundEvent = SoundEvents.FirstOrDefault(x => x._trigger == trigger);
		if (soundEvent._playlist == Playlist.PlayAll)
		{
			foreach (Clip c in soundEvent._clips)		
			{
				PlayClip(c);
			}
		}
		else if (soundEvent._playlist == Playlist.RandomOne)
		{
			int r = UnityEngine.Random.Range(0, soundEvent._clips.Length);
			PlayClip(soundEvent._clips[r]);
		}
	}
	
	[System.Serializable]
	public struct SoundEvent
	{
		public SoundEventTriggers.SoundTriggers _trigger;
		public Playlist _playlist;
		public Clip[] _clips;
	} 
	[System.Serializable]
	public struct Clip
	{
		public AudioClip _audio;
		public float _delay;
		public int _repeat;
		public float _pitch;
		[HideInInspector]
		public List<AudioSource> _audioSources;
	} 
	public enum Playlist
	{
		RandomOne,
		PlayAll
	}
	
	private void PlayClip(Clip c)
	{
		c._repeat = Mathf.Clamp(c._repeat, 1, 5);
		//Debug.Log ("PlayClip audio="+ c._audio.name + " repeats=" + c._repeat);
		if (c._audioSources.Count < c._repeat)
		{
			for(int i=c._audioSources.Count; i<c._repeat; i++)
			{
				c._audioSources.Add(gameObject.AddComponent<AudioSource>());
			}
		}
		
		for(int i=0; i<c._repeat; i++)
		{
			float delay = c._delay;
			if (c._repeat > 1 && i > 0)
			{
				delay += c._audio.length * i;
			}
			
			if (c._audioSources[i].isPlaying)
			{
				c._audioSources[i].Stop();
			}
			
			if (c._pitch > 0)
			{
				c._audioSources[i].pitch = UnityEngine.Random.Range(1f, Mathf.Min(3f, 1f+c._pitch));
			}
			
			//Debug.Log ("PlayClip audio="+ c._audio.name + " delay=" + delay);
			c._audioSources[i].playOnAwake = false;
			c._audioSources[i].clip = c._audio;
			c._audioSources[i].PlayDelayed(delay);
		}
	}
	*/
	#endregion
}









