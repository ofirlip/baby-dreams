﻿using Assets.Scripts.Utils.GameEvents;
using UnityEngine;

namespace Assets.Scripts
{
    public class OptimizationManager : CommandMonoBehaviour
    {
        private Animator[] _animators;
        
        public float MaxXDistanceToBeActive;
        public Camera Camera;

        public void Awake()
        {
            _animators = Resources.FindObjectsOfTypeAll<Animator>();
//			Debug.Log(_animators.Length);
//			for(int i=0;i<_animators.Length;i++)
//				if(_animators[i].gameObject.name == "Tree1")
//					Debug.Log(Mathf.Abs(Camera.transform.position.x - _animators[i].transform.position.x));
            UpdateAnimators();
        }

        public override void Update()
        {
            base.Update();
            UpdateAnimators();
        }

        private void UpdateAnimators()
        {
            foreach (Animator animator in _animators)
            {
				if (Mathf.Abs(Camera.transform.position.x- animator.transform.position.x) > MaxXDistanceToBeActive)
                {
					if (animator.gameObject.tag != "dontDisable")
                        animator.enabled = false;
                }
                else
                {
                    if (!animator.enabled)
                    {
						if (animator.gameObject.tag != "dontDisable")
                       		 animator.enabled = true;
                    }
                }
            }
        }

       
    }
}
