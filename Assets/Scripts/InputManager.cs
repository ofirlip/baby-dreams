﻿using System;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.GameEvents;
using UnityEngine;

namespace Assets.Scripts
{
    public class InputManager : MonoBehaviourBase
    {
        public GameObject Butterfly;
        public GameObject LeftTapArea;
        public GameObject RightTapArea;
        public ButterflyController ButterflyController;
        public bool IsPressAndHold;
		public UIManager UIManagerScript;

        public void OnEnable()
        {
            EasyTouch.On_SimpleTap += OnSimpleTap;

            EasyTouch.On_TouchStart += OnTouchDown;
			EasyTouch.On_TouchDown += OnTouchDown;
            EasyTouch.On_TouchUp += OnTouchUp;

			EasyTouch.On_DragStart += OnDragStart;
			EasyTouch.On_Drag += OnDrag;
			EasyTouch.On_DragEnd += OnDragEnd;
        }

        private void Unsubscribe()
        {
            EasyTouch.On_SimpleTap -= OnSimpleTap;

            EasyTouch.On_TouchStart -= OnTouchDown;
			EasyTouch.On_TouchDown -= OnTouchDown;
       	    EasyTouch.On_TouchUp -= OnTouchUp;

            EasyTouch.On_DragStart -= OnDragStart;
            EasyTouch.On_Drag -= OnDrag;
            EasyTouch.On_DragEnd -= OnDragEnd;
        }

        public void OnDisable()
        {
            Unsubscribe();
        }

        public void OnDestroy()
        {
            Unsubscribe();
        }

		private bool CheckUIOpened()
		{
			return UIManagerScript.UIOpened;
		}

		private void OnTouchDown(BaseFingerGesture gesture)
		{
			if (gesture.pickedObject == null && !CheckUIOpened())
			{
			    IsPressAndHold = true;
			    ButterflyController.OnPositionSelected(gesture.GetTouchToWorldPoint(0));
			}
		}

        private void OnTouchUp(BaseFingerGesture gesture)
        {
			if(!CheckUIOpened())
            	IsPressAndHold = false;
        }

        private void OnSimpleTap(BaseFingerGesture gesture)
        {
			if(!CheckUIOpened())
			{
	            if (gesture.pickedObject == LeftTapArea || gesture.pickedObject == RightTapArea)
	            {
	                MetagameEvents.LookAreaSelected.Publish(
	                    new GameEventArgs<Direction>(gesture.pickedObject == LeftTapArea ? Direction.Left : Direction.Right));
	                return;
	            }

	            if (gesture.pickedObject == Butterfly)
	            {
	                //butterfly tapped
	                return;
	            }

	            if (gesture.pickedObject != null)
	            {
	                Friend friend = gesture.pickedObject.GetComponent<Friend>();
	                if (friend != null)
	                {
	                    MetagameEvents.FriendSelected.Publish(new GameEventArgs<Friend>(friend));
	                    return;
	                }

	                FriendTrigger friendTrigger = gesture.pickedObject.GetComponent<FriendTrigger>();
	                if (friendTrigger != null)
	                {
	                    MetagameEvents.FriendSelected.Publish(new GameEventArgs<Friend>(friendTrigger.Friend));
	                    return;
	                }

					FriendFromGroup friendFromGroup = gesture.pickedObject.GetComponent<FriendFromGroup>();
					if(friendFromGroup != null)
					{
						MetagameEvents.FriendFromGroupSelected.Publish(new GameEventArgs<FriendFromGroup>(friendFromGroup));
						return;
					}

	                TapableItem tapableItem = gesture.pickedObject.GetComponent<TapableItem>();
	                if (tapableItem != null)
	                {
	                    MetagameEvents.TapableItemSelected.Publish(new GameEventArgs<TapableItem>(tapableItem));
	                    return;
	                }
	            }

	            ButterflyController.OnPositionSelected(gesture.GetTouchToWorldPoint(0));
			}
        }

        #region Drag and Drop

        private bool _isDragStarted;

        private void OnDragStart(BaseFingerGesture gesture)
        {
			if (!CheckUIOpened() && GameStateManager.MovingState != MovingState.FriendMeeting && gesture.pickedObject == Butterfly)
            {
                _isDragStarted = true;
                MetagameEvents.ButterflyDragStarted.Publish(GameEventArgs.Empty);
            }
        }

        private void OnDrag(BaseFingerGesture gesture)
        {
			if (!CheckUIOpened() && GameStateManager.MovingState != MovingState.FriendMeeting && gesture.pickedObject == Butterfly && _isDragStarted)
            {
                MetagameEvents.ButterflyDrag.Publish(new GameEventArgs<Vector2>(gesture.GetTouchToWorldPoint(0)));
            }
        }

        private void OnDragEnd(BaseFingerGesture gesture)
        {
			if (!CheckUIOpened() && GameStateManager.MovingState != MovingState.FriendMeeting && gesture.pickedObject == Butterfly && _isDragStarted)
            {
                MetagameEvents.ButterflyDragFinished.Publish(GameEventArgs.Empty);
                _isDragStarted = false;
            }
        }

        #endregion
    }
}
