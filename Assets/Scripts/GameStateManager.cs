﻿using Assets.Scripts.Utils.GameEvents;
using UnityEngine;

namespace Assets.Scripts
{
    public class GameStateManager : CommandMonoBehaviour
    {
        private static MovingState _movingState;
        public static MovingState MovingState
        {
            get { return _movingState; }
            set
            {
                bool isChanged = _movingState != value;
                if (isChanged)
                {
                    _movingState = value;
                    MetagameEvents.MovingStateChanged.Publish(new GameEventArgs<MovingState>(_movingState));
					//Debug.Log("State changed: " + _movingState);
                }
            }
        }

        public void Awake()
        {
            MovingState = MovingState.Idle;
        }
    }

    public enum MovingState
    {
        Idle,
        Flying,
        FriendMeeting,
        LookingLeft,
        LookingRight,
        Action
    }
}
