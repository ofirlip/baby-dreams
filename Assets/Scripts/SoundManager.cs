﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

public class SoundManager : MonoBehaviour 
{
	public AudioClip[] flowerSounds; 

	///private GameManager _gameManager;
	private Transform _callingObj = null;
	private bool _loadedGeneralSounds = false;
	private Dictionary<string, bool> _loadedLanguageSounds = new Dictionary<string, bool>();
	private Dictionary<string, AudioSource> _audioSources = new Dictionary<string, AudioSource>();
	private Dictionary<SoundTriggers, string> _clipMapping = new Dictionary<SoundTriggers, string>();
	public enum SoundTriggers
	{
		Sun,
		Cloud,
		Rainbow,
		Flower,
		AnimalClick,
		AnimalNameClick,
		FoodDrop,
		FoodReturnToPosition,
		FoodEaten,
		FoodFeedingComplete,
		MoreFood,
		DisplayFoodCounter,
		SmallBirdEgg,
		Victory
	}
	
	public void InitSound()
	{
		//Debug.Log("InitSound");
		
		///_gameManager = GameObject.Find("GameManager").gameObject.GetComponent<GameManager>();
		_clipMapping.Add(SoundTriggers.Flower, "Bubble");
		_clipMapping.Add(SoundTriggers.SmallBirdEgg, "SmallBirdEgg");
		_clipMapping.Add(SoundTriggers.Victory, "Victory");
		_clipMapping.Add(SoundTriggers.FoodDrop, "Slide Up Whistle");
		_clipMapping.Add(SoundTriggers.Cloud, "Electronic key");
		_clipMapping.Add(SoundTriggers.FoodReturnToPosition, "Bouncing2");
		_clipMapping.Add(SoundTriggers.MoreFood, "Vocal_ClickForMoreFood");
		_clipMapping.Add(SoundTriggers.FoodEaten, "Eat");
		
		LoadAudioSources();
	}
	
	public void LoadAudioSources()
	{
		//Debug.Log("LoadAudioSources");
		
		// exit if already been loaded
		if (_loadedGeneralSounds)
		{
			//Debug.Log("Already loaded general sounds");
			return;
		}
		
		// non language dependant
		_audioSources.Add("Little Girl Giggles 0201", SetAudioSource("Little Girl Giggles 0201"));
		_audioSources.Add("Little Girl Giggles 0202", SetAudioSource("Little Girl Giggles 0202"));
		_audioSources.Add("Little Girl Giggles 0204", SetAudioSource("Little Girl Giggles 0204"));
		_audioSources.Add("Little Girl Giggles 0205", SetAudioSource("Little Girl Giggles 0205"));
		_audioSources.Add("Little Girl Giggles 0206", SetAudioSource("Little Girl Giggles 0206"));
		_audioSources.Add("LittleGirlGiggles", SetAudioSource("LittleGirlGiggles"));
		_audioSources.Add("Slide Up Whistle", SetAudioSource("Slide Up Whistle"));
		_audioSources.Add("TaDa-1884170640", SetAudioSource("TaDa-1884170640"));
		_audioSources.Add("Electronic key", SetAudioSource("Electronic key"));
		_audioSources.Add("SFXcartoon017", SetAudioSource("SFXcartoon017"));
		_audioSources.Add("Bouncing2", SetAudioSource("Bouncing2"));
		_audioSources.Add("Bubble", SetAudioSource("Bubble"));
		_audioSources.Add("Eat", SetAudioSource("Eat"));
		_audioSources.Add("SmallBirdEgg", SetAudioSource("SmallBirdEgg"));
		_audioSources.Add("Victory", SetAudioSource("Victory"));
		
		/*foreach(AnimalScreen s in _gameManager._screens)
		{
			// for each animal, load the Big and Small animal sounds
			_audioSources.Add("Big"+s._animalRoot.name, SetAudioSource("Big"+s._animalRoot.name));
			_audioSources.Add("Small"+s._animalRoot.name, SetAudioSource("Small"+s._animalRoot.name));
		}
		_loadedGeneralSounds = true;
		
		//Debug.Log(_gameManager._languages[3]);
		
		foreach(string lang in _gameManager._languages)
		{
			_loadedLanguageSounds.Add(lang,false);
			LoadAudioSourcesByLanguage(lang);
		}*/
	}
	
	public void LoadAudioSourcesByLanguage(string lang)
	{
		//Debug.Log("LoadAudioSourcesByLanguage " + lang);
		
		// exit if language has already been loaded
		if (_loadedLanguageSounds[lang]) 
		{
			//Debug.Log("Already loaded " + lang);
			return;
		}
		
		/*foreach(AnimalScreen s in _gameManager._screens)
		{
			// for each animal, load the Vocal sounds
			_audioSources.Add("vocal_Big"+s._animalRoot.name+"_"+lang, SetAudioSource("vocal_Big"+s._animalRoot.name, lang));
			_audioSources.Add("vocal_Small"+s._animalRoot.name+"_"+lang, SetAudioSource("vocal_Small"+s._animalRoot.name, lang));
		}*/
		
		// vocals for number counting 1-10
		for (int i=1;i<=10;i++)
		{
			_audioSources.Add("vocal_"+i+"_"+lang, SetAudioSource("vocal_"+i, lang));
		}
		_audioSources.Add("Vocal_TakeAPhotoWithTheAnimal_"+lang, SetAudioSource("Vocal_TakeAPhotoWithTheAnimal", lang));
		_audioSources.Add("Vocal_ClickForMoreFood_"+lang, SetAudioSource("Vocal_ClickForMoreFood", lang));
		_audioSources.Add("Vocal_BabyAnimals_"+lang, SetAudioSource("Vocal_BabyAnimals", lang));
		_audioSources.Add("Vocal_ForParents_"+lang, SetAudioSource("Vocal_ForParents", lang));
		//Debug.Log ("adding Vocal_ForParents_"+lang);
		_audioSources.Add("Vocal_LibiLabs_"+lang, SetAudioSource("Vocal_LibiLabs", lang));
		_audioSources.Add("Vocal_WellDone_"+lang, SetAudioSource("Vocal_WellDone", lang));
		_audioSources.Add("Vocal_Terrific_"+lang, SetAudioSource("Vocal_Terrific", lang));
		_audioSources.Add("Vocal_GoodJob_"+lang, SetAudioSource("Vocal_GoodJob", lang));
		_audioSources.Add("Vocal_SuchFun_"+lang, SetAudioSource("Vocal_SuchFun", lang));
		_audioSources.Add("Vocal_Super_"+lang, SetAudioSource("Vocal_Super", lang));
		_audioSources.Add("Vocal_Play_"+lang, SetAudioSource("Vocal_Play", lang));
		_audioSources.Add("Vocal_GetAllAnimals_"+lang, SetAudioSource("Vocal_GetAllAnimals", lang));
		
		_loadedLanguageSounds[lang] = true;
	}
	private AudioSource SetAudioSource(string clipName)
	{
		return SetAudioSource(clipName, string.Empty);
	}
	private AudioSource SetAudioSource(string clipName, string lang)
	{
		string path = lang == string.Empty ? clipName : lang+"/"+clipName;
		AudioClip c = null;
		try 
		{
			c = Resources.Load<AudioClip>(path);
		}
		catch(Exception ex)
		{
			//Debug.Log("Error loading clip: " +  clipName + " path: " + path);
		}
		
		if (c == null)
		{
			return null;
		}
		
		AudioSource a = gameObject.AddComponent<AudioSource>();
		a.clip = c;
		a.playOnAwake = false;
		return a;
	}
	
	public void PlayFX(SoundTriggers trigger)
	{
		PlayFX(trigger, null);	
	}
	public void PlayFX(SoundTriggers trigger, Hashtable args)
	{
		///if (!_gameManager._VOICE) return;
		
		switch(trigger)
		{
		case SoundTriggers.Sun:
			PlayRandomClip(new string[]{"Little Girl Giggles 0201","Little Girl Giggles 0202","Little Girl Giggles 0204","Little Girl Giggles 0205","Little Girl Giggles 0206"});
			break;
		case SoundTriggers.Flower:
			//	PlayAudioClip(_clipMapping[SoundTriggers.Flower], Utils.Hash("pitchRange", 2f));
			int clipNumber = UnityEngine.Random.Range(0, flowerSounds.Length);
			GetComponent<AudioSource>().clip = flowerSounds[clipNumber];
			GetComponent<AudioSource>().Play();
			break;
		case SoundTriggers.Rainbow:
			//GetComponent<AudioSource>().clip = rainbowSound;
			GetComponent<AudioSource>().Play();
			break;
		case SoundTriggers.SmallBirdEgg:
			PlayAudioClip(_clipMapping[SoundTriggers.SmallBirdEgg]);
			break;
		case SoundTriggers.Victory:
			PlayAudioClip(_clipMapping[SoundTriggers.Victory]);
			break;
		case SoundTriggers.FoodDrop:
			PlayAudioClip(_clipMapping[SoundTriggers.FoodDrop]);
			break;
		case SoundTriggers.MoreFood:
			PlayAudioClip(_clipMapping[SoundTriggers.MoreFood]);
			break;
		case SoundTriggers.FoodFeedingComplete:
			PlayAudioClip("TaDa-1884170640");
			PlayRandomClip(new string[]{"Vocal_Terrific","Vocal_GoodJob"}, args);
			break;
		case SoundTriggers.Cloud:
			PlayAudioClip(_clipMapping[SoundTriggers.Cloud], Utils.Hash("pitchRange", 1f));
			break;
		case SoundTriggers.FoodReturnToPosition:
			//GetComponent<AudioSource>().clip = bouncing;
			GetComponent<AudioSource>().Play();
			//PlayRandomClip(new string[]{"SFXcartoon017","Bouncing2"}, Utils.Hash("delay",0.2f));
			break;
		case SoundTriggers.DisplayFoodCounter:
			PlayAudioClip("vocal_" + args["number"].ToString());
			break;
		case SoundTriggers.AnimalClick:
			PlayAudioClip(args["animalName"].ToString(), args);
			break;
		case SoundTriggers.AnimalNameClick:
			PlayAudioClip("vocal_" + args["animalName"].ToString());
			break;
		}
	}
	
	public int PlayRandomClip(string[] clipNames)
	{
		int r = UnityEngine.Random.Range(0,clipNames.Length);
		PlayAudioClip(clipNames[r]);
		return r;
	}
	public int PlayRandomClip(string[] clipNames, Hashtable args)
	{
		int r = UnityEngine.Random.Range(0,clipNames.Length);
		PlayAudioClip(clipNames[r], args);
		return r;
	}
	public void PlayAudioClip(string clipName)
	{
		Debug.Log ("PlayAudioClip " + clipName);
		PlayAudioSource(clipName, 0f, false, 0f);
	}
	public void PlayAudioClip(string clipName, Hashtable args)
	{
		if (args.Count == 0) return;
		
		float delay = 0f;
		bool loop = false;
		float pitchRange = 0f;
		
		
		if (args.Contains("delay") && args["delay"].GetType() == typeof(float)) 
		{
			delay = (float)args["delay"];
		}
		if (args.Contains("callingObj") && args["callingObj"].GetType() == typeof(Transform)) 
		{
			_callingObj = (Transform)args["callingObj"];
		}
		if (args.Contains("loop") && args["loop"].GetType() == typeof(bool)) 
		{
			loop = (bool)args["loop"];
		}
		if (args.Contains("pitchRange") && args["pitchRange"].GetType() == typeof(float)) 
		{
			pitchRange = (float)args["pitchRange"];
		}
		
		PlayAudioSource(clipName, delay, loop, pitchRange);
	}
	private void PlayAudioSource(string clipName, double delay, bool loop, float pitchRange)
	{
		//Debug.Log("PlayAudioSource:1: " + clipName);
		AudioSource a = null;
		if (clipName.ToUpper().StartsWith("VOCAL_"))
		{
			//Debug.Log("PlayAudioSource:2: " + clipName+"_"+_gameManager._LANG);
			///if (_audioSources[clipName+"_"+_gameManager._LANG] == null) return;
			///a = _audioSources[clipName+"_"+_gameManager._LANG];
			//Debug.Log("PlayAudioSource:3: " + clipName+"_"+_gameManager._LANG);
			
		}
		else 
		{
			if (_audioSources[clipName] == null) return;
			a = _audioSources[clipName];
			
		}
		//AudioSource a = _audioSources.Find(delegate(AudioSource s) { return s.clip.name.ToUpper() == clipName.ToUpper(); });
		
		if (a != null)
		{
			a.loop = loop;
			if (pitchRange > 0)
			{
				a.pitch = UnityEngine.Random.Range(1f, Mathf.Min(3f, 1f+pitchRange));
			}
			a.Play((ulong)delay*44100);
		}
	}
	
	
	public AudioSource GetAudioSource(SoundTriggers trigger)
	{
		//return _audioSources.Find(delegate(AudioSource s) { return s.clip.name.ToUpper() == _clipMapping[trigger].ToUpper(); });
		return _audioSources[_clipMapping[trigger]];
	}
}
