﻿using UnityEngine;
using Assets.Scripts.Utils;
using System.Collections;

namespace Assets.Scripts
{
	public class FriendsGroup : MonoBehaviourBase
	{
		[HideInInspector]
		public FriendFromGroup[] Friends;

		public Transform LandingPivot;
//		public Transform MagicPivot;
		public Transform CameraPivot;
		public float CameraFocusSize;
		public Direction ButterflyLookAt;
		public OnFriendReaction ButterflyReaction;
		public bool AlreadyClicked;

		public void Awake()
		{
			Friends = GetComponentsInChildren<FriendFromGroup>(true);
			foreach(FriendFromGroup friend in Friends)
				friend.Group = this;
		}

		public MeetingSettings GetMeetingSettings()
		{
			return new MeetingSettings
			{
				LandingPivot = LandingPivot,
				CameraPivot = CameraPivot,
				CameraFocusSize = CameraFocusSize,
				ButterflyLookAt = ButterflyLookAt,
				ButterflyReaction = ButterflyReaction
//				MagicPivot = MagicPivot
			};
		}
	}
}
