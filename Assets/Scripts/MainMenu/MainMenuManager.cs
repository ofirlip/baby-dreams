﻿using System;
using System.Collections;
using System.Linq;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.GameEvents;
using UnityEngine;
using System.Collections.Generic;


namespace Assets.Scripts
{
	public class MainMenuManager : CommandMonoBehaviour 
	{
		public Animator		HomeScreenButterflyAnimator;
		public bool			HomeScreenMenuSelected;

		public BedRoomButterflyManager	BedRoomManager;
		public ButterflyController		ButterflyController;
		public HouseManager				HouseManager;

		public Vector3		HomeScreenUIPos;
		public Vector3		BedRoomScreenUIPos;
		public Camera		MainCamera;
		public GameObject	HomeScreenObj;

		public bool SkipBedroomScene = true;
		
		private AnimatorEvents butterFlyAnimatorEvents;

		void Awake()
		{
			butterFlyAnimatorEvents = new AnimatorEvents(HomeScreenButterflyAnimator);
		}

		void Start () 
		{
			HomeScreenMenuSelected=false;
			MainCamera.transform.localPosition = HomeScreenUIPos;
			if(PlayerPrefs.GetString("GetToBedroom","No")=="Yes")
			{
				HomeScreenObj.SetActive(false);
				SkipBedroomScene=false;
				StartCoroutine(SunSelected(0f));
				PlayerPrefs.SetString("GetToBedroom","No");
			}
		}
		
		public void OnTap(TapGesture gesture)
		{
			if(!HomeScreenMenuSelected)
			{
				GameObject _tappedObject = gesture.Selection;
				if (gesture.Selection != null && 
				    (_tappedObject.name == "SunCollider" || _tappedObject.name == "MoonCollider"))
				{
					if ((_tappedObject.name == "SunCollider"))
					{
						StartCoroutine(SunSelected(2.4f));
					}
					else if((_tappedObject.name == "MoonCollider"))
					{
						StartCoroutine(MoonSelected());
					}
					HomeScreenMenuSelected = true;
				}
			}
		}
		
		public IEnumerator SunSelected(float time)
		{
			if (SkipBedroomScene)
			{
				GameObject.FindGameObjectWithTag("ButterflyController").GetComponent<ButterflyController>().LoadLevelDay();
				yield return new WaitForSeconds(0.1f);
			}
			else 
			{
				HomeScreenMenuSelected = false;
				HomeScreenButterflyAnimator.SetTrigger("SunSelected");
				yield return new WaitForSeconds(time);
				MainCamera.transform.localPosition = BedRoomScreenUIPos;
				BedRoomManager.BedRoomActivate();
			}
		}
		
		private IEnumerator MoonSelected()
		{
			HomeScreenMenuSelected = false;
			HomeScreenButterflyAnimator.SetTrigger("MoonSelected");
			yield return new WaitForSeconds(2.4f);

			HouseManager.ActivateGame();
			ButterflyController.LoadLevelNight();
		}

		public void BedRoomActivateCamera()
		{
			HomeScreenMenuSelected = false;
			MainCamera.transform.localPosition = BedRoomScreenUIPos;
		}

	}
//-16.7,4.4,-5
}
