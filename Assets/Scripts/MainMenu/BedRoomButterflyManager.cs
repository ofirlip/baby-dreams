﻿using System;
using System.Collections;
using System.Linq;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.GameEvents;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts
{
	public class BedRoomButterflyManager : CommandMonoBehaviour 
	{
		public SoundsManager SoundsManagerObj;
		public Animator 	BedRoomScreenButterflyAnimator;
		public Animator 	BedRoomScreenWindowAnimator;
		public Animator 	BedRoomScreenSoapAnimator;
		public Animator 	BedRoomScreenTeethBrushAnimator;
		public Animator		BedRoomScreenAllFoodAnimator;
		public Animator		BedRoomScreenConfettiAnimator;

		public Transform	BedRoomScreenTapHint;
		public GameObject	SoapParticleParent;
		public GameObject[]	AllFoodArray;

		public Camera			GamePlayCamera;
		public Camera			BedroomCamera;
		public MainMenuManager	MainMenuManagerObj;

		public int 		NightToDayActivationTimer;//when the window animation should start since scene start
		public int 		NagStartTimer;//should be >10 and <HintsTimer[0]
		public int 		SadStartTimer;//default is >5
		public int 		SoapWashingTime;//default is 3
		public int 		TeethBrushingTime;//default is 3
		public int 		BeforeEatingTimer;//default is 4
		public int 		AfterEatingTimer;//default is 1
		public int[] 	HintsTimer; //0 - wakeup timer should be > NagStartTimer, 1 - soap timer, 2 - teeth timer, 3 - food timer

		private Vector2		dragFirstPos;
		private Vector3[] 	hintLocations = new [] { new Vector3(-22.55f,-4f,0f), 
														new Vector3(-22.55f,0.5f,0f), 
															new Vector3(-22.55f,-1.5f,0f),
																new Vector3(-23f,-1.5f,0f),
																	new Vector3(-22.6f,-1.5f,0f)}; //0- Hint Starting pos, 1 - WakeUp Hint , 2 - SoapClean Hint, 3 - TeethBrush Hint, 4 - EatFood Hint

		private AnimatorEvents butterFlyAnimatorEvents;
		private AnimatorEvents soapAnimatorEvents;

		private float 		bedRoomNoTapTimer;
		private int 		bedRoomNoTapTimerTrigger;
		private bool		bedRoomScreenTapped;
		private float		bedRoomCleaningTimer;

		private bool		bedRoomCleaningFinished;
		private bool		bedRoomTeethBrushFinished;
		private bool		bedRoomEatingFinished;
		private bool		bedRoomSleepFinished;
		private bool		sadPlayedOneTime;
		private bool		bedroomNightStance;

		private Vector3[]	soapElementsPos;
		private Vector3[]	brushingTheetElementPos;
	
		public enum TapWaitTriggers { BedWakeUp, SoapClean, TeethBrush, EatFood}
		private TapWaitTriggers _tapWaitTriggers;

		public float 	Narrator01WaitTimer;
		public float	Narrator02WaitTimer;
		public float	Narrator03WaitTimer;
		public float	Narrator04WaitTimer;
		public float	Narrator05WaitTimer;
		public float	Narrator05_2WaitTimer;
		public float	Narrator24WaitTimer;
		public float	Narrator31WaitTimer;
		public float	Narrator33WaitTimer;

		void Awake()
		{
			butterFlyAnimatorEvents = new AnimatorEvents(BedRoomScreenButterflyAnimator);
		}

		void Start () 
		{
			soapElementsPos = new Vector3[3];
			brushingTheetElementPos = new Vector3[2];
			bedRoomSleepFinished = true;
			bedRoomScreenTapped = true;
			BedRoomScreenButterflyAnimator.enabled = false;
//			StartCoroutine(SoundsManagerPlaySoundWait(Narrator01WaitTimer,"01HomeScreen"));
			soapElementsPos[0] = BedRoomScreenSoapAnimator.transform.FindChild("SoapBowl").localPosition;
			soapElementsPos[1] = BedRoomScreenSoapAnimator.transform.FindChild("SpongeAll").localPosition;
			soapElementsPos[2] = BedRoomScreenSoapAnimator.transform.FindChild("SpongeAll").FindChild("Sponge").localPosition;
			brushingTheetElementPos[0] = BedRoomScreenTeethBrushAnimator.transform.GetChild(0).localPosition;
			brushingTheetElementPos[1] = BedRoomScreenTeethBrushAnimator.transform.GetChild(0).GetChild(0).localPosition;
		}

		void SoundsManagerPlaySound( string clipName)
		{
			SoundsManager.Play(clipName);
		}

		IEnumerator SoundsManagerPlaySoundWait(float timer, string clipName)
		{
			yield return new WaitForSeconds(timer);
			SoundsManager.Play(clipName);
		}

		IEnumerator SoundsManagerPlaySoundWaitCheckTapped(float timer, string clipName, TapWaitTriggers trigger)
		{
			yield return new WaitForSeconds(timer);
			if(!bedRoomScreenTapped && _tapWaitTriggers == trigger)
				SoundsManager.Play(clipName);
		}
		
		void Update()
		{
			if(!bedRoomScreenTapped)
			{
				bedRoomNoTapTimer += Time.deltaTime;
				if(bedRoomNoTapTimer>bedRoomNoTapTimerTrigger)
				{
					NoTapTimerTrigger();
				}
				if(bedRoomNoTapTimer> SadStartTimer 
				   && _tapWaitTriggers != TapWaitTriggers.BedWakeUp 
				   && !sadPlayedOneTime)
				{
					SadBehavior();
				}
			}
		}

		public void BedRoomActivate()
		{
			StartCoroutine(SoundsManagerPlaySoundWait(Narrator02WaitTimer,"02NightEnds"));
			StartCoroutine(SoundsManagerPlaySoundWait((Narrator02WaitTimer+Narrator03WaitTimer),"03LetsWakeOurFriend"));
			StartCoroutine(SoundsManagerPlaySoundWaitCheckTapped((Narrator02WaitTimer+Narrator03WaitTimer+Narrator04WaitTimer),"04RiseAndShine",TapWaitTriggers.BedWakeUp));
			StartCoroutine(SoundsManagerPlaySoundWaitCheckTapped((Narrator02WaitTimer+Narrator03WaitTimer+Narrator04WaitTimer+Narrator05WaitTimer),"05WakeUp",TapWaitTriggers.BedWakeUp));


			BedRoomScreenButterflyAnimator.SetTrigger("EntrySleep");
			BedRoomScreenButterflyAnimator.enabled = true;
			bedRoomScreenTapped = false;
			ResetTimer(NightToDayActivationTimer,TapWaitTriggers.BedWakeUp);

		}

		private void NoTapTimerTrigger()
		{
			switch(_tapWaitTriggers)
			{
			case TapWaitTriggers.BedWakeUp:

				if(bedRoomNoTapTimerTrigger == NightToDayActivationTimer)
				{
					if(bedroomNightStance)
					{
						SoundsManagerPlaySound("34DayBedroom1");
						StartCoroutine(SoundsManagerPlaySoundWait(9f,"35LetsWakeToANewDay"));
						StartCoroutine(SoundsManagerPlaySoundWaitCheckTapped((8f+Narrator04WaitTimer),"04RiseAndShine",TapWaitTriggers.BedWakeUp));
						StartCoroutine(SoundsManagerPlaySoundWaitCheckTapped((8f+Narrator04WaitTimer+Narrator05WaitTimer),"05WakeUp",TapWaitTriggers.BedWakeUp));

						bedroomNightStance = false;
						ResetTimer(HintsTimer[0],TapWaitTriggers.BedWakeUp);
					}

					BedRoomScreenWindowAnimator.SetTrigger("NightToDay");
					bedRoomNoTapTimerTrigger = NagStartTimer;
				}
				else if(bedRoomNoTapTimerTrigger == NagStartTimer)
				{
					BedRoomScreenButterflyAnimator.SetTrigger("ButterflySleepsNag");
					bedRoomNoTapTimerTrigger = HintsTimer[0];
				}
				else if(bedRoomNoTapTimerTrigger == HintsTimer[0])
				{
					ActivateHint(0);

				}
				break;

			case TapWaitTriggers.SoapClean:
				if(bedRoomNoTapTimerTrigger == HintsTimer[1])
				{
					ActivateHint(1);
				}
				break;
			case TapWaitTriggers.TeethBrush:
				if(bedRoomNoTapTimerTrigger == HintsTimer[2])
				{
					ActivateHint(2);
				}
				break;
			case TapWaitTriggers.EatFood:
				if(bedRoomNoTapTimerTrigger == HintsTimer[3])
				{
					ActivateHint(3);
				}
				break;
			}
		}

		public void OnTap(TapGesture gesture)
		{
			sadPlayedOneTime = false;
			if(gesture.Selection != null && gesture.Selection.name == "ButterflyAllCollider")
			{
				StartCoroutine( ButterFlyBehaviorTap());
			}
		}

		public void OnDrag(DragGesture gesture)
		{
			sadPlayedOneTime = false;

			GameObject startObject = gesture.StartSelection;
			GameObject finishObject = gesture.Selection;

			if(gesture.Phase == ContinuousGesturePhase.Started && gesture.Raycast.Hit2D )
			{
				DragStart(startObject,gesture); 
			}
			else if(gesture.Phase == ContinuousGesturePhase.Updated && gesture.Raycast.Hit2D )
			{
				DragUpdate(startObject,gesture);
			}
			else if(gesture.Phase == ContinuousGesturePhase.Ended)
			{
				DragFinished(startObject,finishObject);
			}
		}

		private void DragStart(GameObject startObject, Gesture gesture)
		{
			DeActivateHint();
			if(!bedRoomCleaningFinished && startObject.name == "Sponge")
			{
				SoapParticleParent.SetActive(true);
				BedRoomScreenButterflyAnimator.SetTrigger("ButterflyStartCleaning");
			}
			else if(!bedRoomTeethBrushFinished && startObject.name == "TeethBrushAll")
			{
				dragFirstPos = gesture.Raycast.Hit2D.point;
				BedRoomScreenButterflyAnimator.SetTrigger("ButterflyBrushTeeth");
			}
		}

		private void DragUpdate(GameObject startObject, Gesture gesture)
		{
			if(BedRoomScreenButterflyAnimator.enabled)
			{
				if(!bedRoomCleaningFinished && startObject.name == "Sponge")
				{
					startObject.transform.position = new Vector3(gesture.Raycast.Hit2D.point.x,
					                                             gesture.Raycast.Hit2D.point.y,
					                                             startObject.transform.position.z);
					startObject.transform.localPosition = new Vector3(Mathf.Clamp(startObject.transform.localPosition.x,-4f, 4f), 
					                                                  Mathf.Clamp(startObject.transform.localPosition.y,-0.5f, 4.5f), 0);
					bedRoomCleaningTimer+=Time.deltaTime;
					if(bedRoomCleaningTimer > SoapWashingTime)
					{
						startObject.transform.localPosition = new Vector3(-0.02f,-0.01f,-2);
						StartCoroutine(FinishedCleaning());
					}
				}
				else if(!bedRoomTeethBrushFinished && startObject.name == "TeethBrushAll")
				{
					Vector2 newPos = dragFirstPos - gesture.Raycast.Hit2D.point;
					startObject.transform.position = new Vector3(-859.2f - newPos.x,
					                                             330.8f - newPos.y,
					                                             startObject.transform.position.z);
					startObject.transform.localPosition = new Vector3(Mathf.Clamp(startObject.transform.localPosition.x,-48f, -41f), 
					                                                  Mathf.Clamp(startObject.transform.localPosition.y,-6f, 0f), 0);
					bedRoomCleaningTimer+=Time.deltaTime;
					if(bedRoomCleaningTimer > TeethBrushingTime)
					{
						startObject.transform.localPosition = new Vector3(-45f,-3f,-3f);
						StartCoroutine(FinishedBrushing());
					}
				}
				else if(!bedRoomEatingFinished && startObject.tag == "Food")
				{
					startObject.transform.position = new Vector3(gesture.Raycast.Hit2D.point.x,
					                                             gesture.Raycast.Hit2D.point.y,
					                                             startObject.transform.position.z);
					if(gesture.Selection.name == "MouthCollider")
						StartCoroutine(EatingBehavior(startObject));
				}
			}
		}

		private void DragFinished(GameObject startObject,GameObject finishObject)
		{
			if(BedRoomScreenButterflyAnimator.enabled)
			{
				if(!bedRoomCleaningFinished && startObject.name == "Sponge")
				{
					SoapParticleParent.SetActive(false);
					iTween.MoveTo(startObject,iTween.Hash("position", new Vector3(-0.02f,-0.01f,-2), "islocal", true, "time", 1f));
					BedRoomScreenButterflyAnimator.SetTrigger("ButteflyStopCleaning");
					ResetTimer(HintsTimer[1],TapWaitTriggers.SoapClean);
				}
				else if(!bedRoomTeethBrushFinished && startObject.name == "TeethBrushAll")
				{
					iTween.MoveTo(startObject,iTween.Hash("position", new Vector3(-45f,-3f,-3f), "islocal", true, "time", 1f));
					BedRoomScreenButterflyAnimator.SetTrigger("ButterflyBrushTeethStop");
					ResetTimer(HintsTimer[2],TapWaitTriggers.TeethBrush);
				}
				else if(!bedRoomEatingFinished && startObject.tag == "Food")
				{
					if(finishObject.name == "MouthCollider")
					{
						DeActivateHint();
						StartCoroutine(EatingBehavior(startObject));
					}
					else 
					{
						iTween.MoveTo(startObject,iTween.Hash("position", new Vector3(0f,-0.5f,0), "islocal", true, "time", 1f));
						ResetTimer(HintsTimer[3],TapWaitTriggers.EatFood);
					}
				}
			}
		}

		private IEnumerator ButterFlyBehaviorTap()
		{
			if(bedRoomNoTapTimer>1 && !bedRoomScreenTapped && _tapWaitTriggers == TapWaitTriggers.BedWakeUp)
			{
				DeActivateHint();
				BedRoomScreenButterflyAnimator.SetTrigger("StartWakeUp");
				yield return StartCoroutine(butterFlyAnimatorEvents.WaitExit("WakeUp"));
				SoundsManagerPlaySound("06GoodMorningFriend");

				StartCoroutine(StartCleaning());
			}
			else if(!bedRoomSleepFinished)
			{
				StartCoroutine(StartSleeping());
			}
		}

		private void ResetTimer(int timerFirstTrigger,TapWaitTriggers triggerLocation)
		{
			_tapWaitTriggers = triggerLocation;
			bedRoomScreenTapped = false;
			bedRoomNoTapTimer = 0;
			bedRoomNoTapTimerTrigger = timerFirstTrigger;
			BedRoomScreenTapHint.gameObject.SetActive(false);
		}

		private void DeActivateHint()
		{
			bedRoomNoTapTimer = 0;
			bedRoomScreenTapped = true;
			BedRoomScreenTapHint.gameObject.SetActive(false);
		}

		private void ActivateHint(int location)
		{
			if(!BedRoomScreenTapHint.gameObject.activeSelf)
			{
				if(location == 0)
					StartCoroutine(SoundsManagerPlaySoundWait(Narrator05_2WaitTimer,"05TapToWakeUp"));

				BedRoomScreenTapHint.gameObject.SetActive(true);
				BedRoomScreenTapHint.localPosition = hintLocations[0];
				iTween.MoveTo(BedRoomScreenTapHint.gameObject,iTween.Hash("position", hintLocations[location+1], "islocal", true, "time", 1));
				StartCoroutine(HintActivatedTimer());

			}
		}

		private void SadBehavior()
		{
			BedRoomScreenButterflyAnimator.SetTrigger("ButterflyRoomSad");
			sadPlayedOneTime = true;
		}

		private IEnumerator HintActivatedTimer()
		{
			yield return new WaitForSeconds(4);
			iTween.MoveTo(BedRoomScreenTapHint.gameObject,iTween.Hash("position", hintLocations[0], "islocal", true, "time", 1.5f));
		}

		private IEnumerator StartCleaning()
		{
			yield return new WaitForSeconds(3f);
			if(!bedroomNightStance)
				SoundsManagerPlaySound("07LetsWashUp");
			else SoundsManagerPlaySound("29BeforeBathNight");
			yield return new WaitForSeconds(2);
			ResetTimer(HintsTimer[1],TapWaitTriggers.SoapClean);
			BedRoomScreenSoapAnimator.gameObject.SetActive(true);
			BedRoomScreenSoapAnimator.enabled = true;
			BedRoomScreenButterflyAnimator.SetTrigger("ButerflyBeforeClean");
			yield return StartCoroutine(butterFlyAnimatorEvents.WaitExit("CleanStart"));

		}

		private IEnumerator FinishedCleaning()
		{
			SoundsManagerPlaySound("08AfterWashUp");
			SoapParticleParent.SetActive(false);
			bedRoomCleaningFinished = true;
			bedRoomCleaningTimer = 0;
			BedRoomScreenSoapAnimator.SetTrigger("SoapCleanDone");
			yield return new WaitForSeconds(0.5f);
			BedRoomScreenSoapAnimator.gameObject.SetActive(false);
			BedRoomScreenButterflyAnimator.SetTrigger("ButterFlyFinishedCleaning");
			yield return StartCoroutine(butterFlyAnimatorEvents.WaitExit("ButterFlyFinishedCleaning"));
			StartCoroutine(StartBrushing());

		}

		private IEnumerator StartBrushing()
		{
			yield return new WaitForSeconds(2f);
			if(!bedroomNightStance)
				SoundsManagerPlaySound("09TimeToBrushTeeth");
			else SoundsManagerPlaySound("27BeforeBrushTeeth");

			yield return new WaitForSeconds(2.5f);
			BedRoomScreenTeethBrushAnimator.gameObject.SetActive(true);
			BedRoomScreenTeethBrushAnimator.enabled = true;
			BedRoomScreenButterflyAnimator.SetTrigger("ButterflyBeforeBrushTeeth");
			ResetTimer(HintsTimer[2],TapWaitTriggers.TeethBrush);
		}

		private IEnumerator FinishedBrushing()
		{
//			yield return new WaitForSeconds(0.5f);
			if(!bedroomNightStance)
				SoundsManagerPlaySound("10AfterBrushTeeth");
			else StartCoroutine(SoundsManagerPlaySoundWait(1f,"28ButterflyAfterBrushingNight"));

			bedRoomTeethBrushFinished = true;
			bedRoomCleaningTimer = 0;
			BedRoomScreenTeethBrushAnimator.SetTrigger("BrushFinished");
			yield return new WaitForSeconds(0.2f);
			BedRoomScreenTeethBrushAnimator.gameObject.SetActive(false);
			BedRoomScreenButterflyAnimator.SetTrigger("ButterflyBrushedFinished");
			yield return StartCoroutine(butterFlyAnimatorEvents.WaitExit("ButterflyBrushedFinished"));

			if(bedroomNightStance)
			{
				yield return new WaitForSeconds(4f);
				SoundsManagerPlaySound("30ReadyToBedNight");
				yield return new WaitForSeconds(1f);
				BedRoomScreenButterflyAnimator.SetTrigger("ButterflyGoodFeedback");
				BedRoomScreenConfettiAnimator.SetTrigger("ConfettiAppear");
				yield return new WaitForSeconds(2.5f);
				StartCoroutine(SoundsManagerPlaySoundWait(Narrator31WaitTimer,"31TaptoTackInBed"));
				bedRoomSleepFinished = false;
				DeActivateHint();
			}
			else 
			{
				StartCoroutine(StartEating());
			}
		}

		private IEnumerator StartEating()
		{
			yield return new WaitForSeconds(2f);
			//load level day for testing;
//			GameObject.Find("ButterflyController").GetComponent<ButterflyController>().LoadLevelDay();

			if(!bedroomNightStance)
				SoundsManagerPlaySound("11TimeForBreakfast");
			else 
				SoundsManagerPlaySound("25BeforeFeedNight");
			yield return new WaitForSeconds(3f);
			RandomizeFood();
			yield return new WaitForSeconds(BeforeEatingTimer);
			BedRoomScreenAllFoodAnimator.gameObject.SetActive(true);
			BedRoomScreenAllFoodAnimator.enabled = true;
			BedRoomScreenAllFoodAnimator.SetTrigger("ButteflyFoodIn");
			BedRoomScreenButterflyAnimator.SetTrigger("ButterflyBeforeEat");
			ResetTimer(HintsTimer[3],TapWaitTriggers.EatFood);
		}

		private IEnumerator EatingBehavior(GameObject startObject)
		{
			SoundsManagerPlaySound("12LooksYummi");
			bedRoomEatingFinished = true;
			startObject.transform.localPosition = new Vector3(0f,-0.5f,0);
			startObject.SetActive(false);
			BedRoomScreenButterflyAnimator.SetTrigger(startObject.name);
			yield return StartCoroutine(butterFlyAnimatorEvents.WaitExit("EatingFinished"));
			StartCoroutine(FinishedEating());
		}

		private IEnumerator FinishedEating()
		{
			if(!bedroomNightStance)
				SoundsManagerPlaySound("13GoodWork");
			else SoundsManagerPlaySound("26AfterEatNight");

			BedRoomScreenAllFoodAnimator.SetTrigger("ButteflyFoodOut");
//			yield return new WaitForSeconds(2f);
//			yield return new WaitForSeconds(0.5f);
//			BedRoomScreenAllFoodAnimator.gameObject.SetActive(false);

			if(bedroomNightStance)
			{
				yield return new WaitForSeconds(2);
				StartCoroutine(StartCleaning());
			}
			else 
			{
				yield return new WaitForSeconds(1f);
				BedRoomScreenButterflyAnimator.SetTrigger("ButterflyGoodFeedback");
				BedRoomScreenConfettiAnimator.SetTrigger("ConfettiAppear");
				yield return new WaitForSeconds(AfterEatingTimer);
				yield return new WaitForSeconds(2.2f);
				SoundsManagerPlaySound("14ReadyToStartDay");
				yield return new WaitForSeconds(5.5f);
				SoundsManagerPlaySound("15LetsGo");
				yield return new WaitForSeconds(2.5f);
				DeActivateHint();
//				BedRoomScreenConfettiAnimator.enabled = false;
				GameObject.Find("ButterflyController").GetComponent<ButterflyController>().LoadLevelDay();
			}
		}

		private void RandomizeFood()
		{
			int random = UnityEngine.Random.Range(0,AllFoodArray.Length);
			for(int i=0;i<AllFoodArray.Length;i++)
			{
				if(i==random)
				{
					AllFoodArray[i].SetActive(true);
				}
				else 
				{
					AllFoodArray[i].SetActive(false);
				}
			}
		}

		private IEnumerator StartSleeping() 
		{
			BedRoomScreenButterflyAnimator.SetTrigger("ButteflyGetInTheBed");
			bedRoomSleepFinished = true;
			yield return new WaitForSeconds(2f);
			if(bedroomNightStance)
			{

				SoundsManagerPlaySound("32SleepWellButterflyNight");

				yield return new WaitForSeconds(2.5f);
				SoundsManagerPlaySoundWait(Narrator33WaitTimer,"33SweetDreams");
				yield return new WaitForSeconds(1.5f);
			}
			ResetAllSettingsDay();
		}

		public IEnumerator ReturnToBedroom()
		{
			ResetAllSettingsNight();
			BedRoomScreenWindowAnimator.SetTrigger("NightWindow");
			BedRoomScreenButterflyAnimator.SetTrigger("ButterflyFrontOfBedIdle");

			yield return new WaitForSeconds(1);
			GamePlayCamera.depth = 0;
			BedroomCamera.depth = 1;
			GameObject.Find("ButterflyController").GetComponent<ButterflyController>().StopGamePlay();
			StartCoroutine(SoundsManagerPlaySoundWait(Narrator24WaitTimer,"24ButerlyNightBedroom1"));
			yield return new WaitForSeconds(4f);
			StartCoroutine(StartEating());
		}

		private void ResetAllSettingsNight()
		{
			ResetElementsPos();
			bedRoomCleaningTimer = 0;
			_tapWaitTriggers = TapWaitTriggers.EatFood;
			bedroomNightStance = true;
			BedRoomScreenButterflyAnimator.enabled = true;
			MainMenuManagerObj.BedRoomActivateCamera();
			bedRoomEatingFinished = false;
			bedRoomTeethBrushFinished = false;
			bedRoomCleaningFinished = false;
		}

		private void ResetAllSettingsDay()
		{
			ResetElementsPos();

//			bedroomNightStance = false;
			ResetTimer(NightToDayActivationTimer,TapWaitTriggers.BedWakeUp);
			bedRoomSleepFinished = true;
			bedRoomEatingFinished = false;
			bedRoomTeethBrushFinished = false;
			bedRoomCleaningFinished = false;
			bedRoomScreenTapped = false;
//			bedRoomNoTapTimer = 0;
//			bedRoomNoTapTimerTrigger = NightToDayActivationTimer;
		}

		private void ResetElementsPos()
		{
			BedRoomScreenSoapAnimator.transform.FindChild("SoapBowl").localPosition = soapElementsPos[0];
			BedRoomScreenSoapAnimator.transform.FindChild("SoapBowl").gameObject.SetActive(true);
			BedRoomScreenSoapAnimator.transform.FindChild("SpongeAll").localPosition = soapElementsPos[1];
			BedRoomScreenSoapAnimator.transform.FindChild("SpongeAll").gameObject.SetActive(true);
			BedRoomScreenSoapAnimator.transform.FindChild("SpongeAll").FindChild("Sponge").localPosition = soapElementsPos[2];
			BedRoomScreenTeethBrushAnimator.transform.GetChild(0).localPosition = brushingTheetElementPos[0];
			BedRoomScreenTeethBrushAnimator.transform.GetChild(0).GetChild(0).localPosition = brushingTheetElementPos[1];
		}
	}
}
