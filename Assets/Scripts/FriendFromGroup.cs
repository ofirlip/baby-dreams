﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
	public class FriendFromGroup : TapableItem 
	{
		public float FriendsActivationTimer;
//		public Transform MagicPivot;

		[HideInInspector]
		public FriendsGroup Group;

		[HideInInspector]
		public bool IsActivated;
	}
}
