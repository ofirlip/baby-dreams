﻿using Assets.Scripts.Utils.GameEvents;
using UnityEngine;

namespace Assets.Scripts
{
    public static class MetagameEvents
    {
        public static GameEvent<GameEventArgs<Direction>> LookAreaSelected { get; private set; } 
        public static GameEvent<GameEventArgs> ButterflyDragStarted { get; private set; } 
        public static GameEvent<GameEventArgs<Vector2>> ButterflyDrag { get; private set; }
        public static GameEvent<GameEventArgs> ButterflyDragFinished { get; private set; } 
        
        public static GameEvent<GameEventArgs<MovingState>> MovingStateChanged { get; private set; } 
        
        public static GameEvent<GameEventArgs<Friend>> FriendSelected { get; private set; }
		public static GameEvent<GameEventArgs<FriendFromGroup>> FriendFromGroupSelected { get; private set; }
        
        public static GameEvent<GameEventArgs<int>> ScreenChanged { get; private set; }

        public static GameEvent<GameEventArgs<TapableItem>> TapableItemSelected { get;private set; }

        static MetagameEvents()
        {
            Initialize();            
        }

        private static void Initialize()
        {
            LookAreaSelected = new GameEvent<GameEventArgs<Direction>>();
            ButterflyDragStarted = new GameEvent<GameEventArgs>();
            ButterflyDrag = new GameEvent<GameEventArgs<Vector2>>();
            ButterflyDragFinished = new GameEvent<GameEventArgs>();

            MovingStateChanged = new GameEvent<GameEventArgs<MovingState>>();

            FriendSelected = new GameEvent<GameEventArgs<Friend>>();
			FriendFromGroupSelected = new GameEvent<GameEventArgs<FriendFromGroup>>();
 
            ScreenChanged = new GameEvent<GameEventArgs<int>>();

            TapableItemSelected = new GameEvent<GameEventArgs<TapableItem>>();
        }
    }
}
