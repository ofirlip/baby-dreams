﻿using System;
using System.Collections;
using System.Linq;
using Assets.Scripts.Utils;
using Assets.Scripts.Utils.GameEvents;
using UnityEngine;
using System.Collections.Generic;
using Prime31;

namespace Assets.Scripts
{
	public class ButterflyCollisionDetect : MonoBehaviour 
	{
		public ButterflyController 	butterflyController;
		private GameObject			currentObj;
		public bool					CollisionDetectActive;
		void OnTriggerEnter2D(Collider2D obj)
		{
			if(CollisionDetectActive)
			{
				if(obj.GetComponent<Friend>() != null )
					butterflyController.OnFriendSelected(obj.GetComponent<Friend>());
				else if(obj.GetComponent<FriendFromGroup>() != null )
					butterflyController.OnFriendFromGroupSelected(obj.GetComponent<FriendFromGroup>());
				else if(obj.GetComponent<TapableItem>() != null )
					StartCoroutine(butterflyController.OnTapableItemSelected(obj.GetComponent<TapableItem>()));
			}
		}

	}
}
