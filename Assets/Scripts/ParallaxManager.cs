﻿using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts
{
    public class ParallaxManager : MonoBehaviourBase
    {
        private Vector2 _cameraStartPosition;
        private Vector2[] _itemsStartPositions;

		public bool _parallax = true;
        public Transform[] ParallaxItems;
        public Vector2[] ParallaxSpeeds;
        public Camera Camera;

        public void Awake()
        {
            _cameraStartPosition = Camera.transform.position;
            _itemsStartPositions = new Vector2[ParallaxItems.Length];
            for (int i = 0; i < ParallaxItems.Length; i++)
			{
                _itemsStartPositions[i] = ParallaxItems[i].transform.position;
			}
        }

        //Use LateUpdate there as we need that camera position updated (from CharacterCamera script)
        //before we start update parallax.
        public void LateUpdate()
        {
			if (_parallax) 
			{
	            for (int i = 0; i < ParallaxItems.Length; i++)
				{
//					Debug.Log(i);
	               /* 
	                ParallaxItems[i].SetXY(
	                    _itemsStartPositions[i].x + (Camera.transform.position.x - _cameraStartPosition.x)*ParallaxSpeeds[i].x,
	                    _itemsStartPositions[i].y + (Camera.transform.position.y - _cameraStartPosition.y)*ParallaxSpeeds[i].y);
	               */
					float x = _itemsStartPositions[i].x + (Camera.transform.position.x - _cameraStartPosition.x)*ParallaxSpeeds[i].x;
					float y = _itemsStartPositions[i].y + (Camera.transform.position.y - _cameraStartPosition.y)*ParallaxSpeeds[i].y;
					ParallaxItems[i].position = Vector3.Slerp(ParallaxItems[i].position, new Vector3(x,y,0f), 1.0f);
				}
			}
        }
    }
}
