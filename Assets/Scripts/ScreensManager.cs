﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Utils.GameEvents;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Assets.Scripts
{
    public class ScreensManager : CommandMonoBehaviour
    {
        private ScreenItem[] _screenItems;
		private int _currentScreen = -1;

        public void Awake()
        {       
			Subscribe(MetagameEvents.ScreenChanged, _ => OnScreenChanged(_.Value));
            _screenItems = FindObjectsOfType<ScreenItem>();
            MetagameEvents.ScreenChanged.Publish(new GameEventArgs<int>(1));
        }

        private void OnScreenChanged(int number)
        {
            foreach (ScreenItem screen in _screenItems)
            {
				if (Mathf.Abs(screen.ScreenNumber - number) <= 1)
				{
					screen.gameObject.SetActive(true);
					if(screen.name.StartsWith("Elements_"))
					{

						foreach (Animator a in screen.transform.gameObject.GetComponentsInChildren<Animator>(true))
						{
//							Debug.Log("screen " +screen + " " + ButterflyController._isDay + " " +a.name);
							a.SetBool("isDay", ButterflyController._isDay);
						}
					}

					if(_currentScreen!=number)
					{
						foreach (Friend friend in screen.transform.GetComponentsInChildren<Friend>(true))
							friend.AlreadyClicked = false;
						foreach (FriendsGroup friend in screen.transform.GetComponentsInChildren<FriendsGroup>(true))
							friend.AlreadyClicked = false;
						foreach (TapableItem friend in screen.transform.GetComponentsInChildren<TapableItem>(true))
							friend.AlreadyClicked = false;
						_currentScreen = number;
					}
				}
				else
				{
					screen.gameObject.SetActive(false);
				}
            }
        }

        #region Realign screen items

        private static int _movingItemsCount;
        private static ScreenBorder[] _screenBorders;
        private static int _currentParallaxIndex;
        private static ParallaxManager _parallaxManager;

		#if UNITY_EDITOR
		[MenuItem("Baby Dream/Realign Screen Items")]
		#endif
        public static void ApplyParallax()
        {
            _movingItemsCount = 0;
            _screenBorders = FindObjectsOfType<ScreenBorder>().OrderBy(_ => _.ScreenNumber).ToArray();
            _parallaxManager = FindObjectOfType<ParallaxManager>();
            for (int i = 0; i < _parallaxManager.ParallaxItems.Length; i++)
            {
                _currentParallaxIndex = i;
                Transform layer = _parallaxManager.ParallaxItems[i];
                ScreenItem[] items = layer.GetComponentsInChildren<ScreenItem>();
                var groups = GroupItems(items);
                foreach (var group in groups)
				{
                    RealignGroup(group.Value);
				}
            }
            _screenBorders = null;
            _parallaxManager = null;
        }

        private static Dictionary<Transform, List<ScreenItem>> GroupItems(ScreenItem[] items)
        {
            var groups = new Dictionary<Transform, List<ScreenItem>>();
            foreach (ScreenItem item in items)
            {
                if (!groups.ContainsKey(item.Transform.parent))
				{
					groups.Add(item.Transform.parent, new List<ScreenItem>());
				}
                groups[item.Transform.parent].Add(item);
            }
            return groups;
        }

        private static void RealignGroup(List<ScreenItem> screenItems)
        {
            foreach (ScreenItem screenItem in screenItems)
            {
				//Debug.Log (screenItem.transform.name + " --- " + screenItems.Count + " items");
				
                if (screenItem.GetComponent<SpriteRenderer>() != null && screenItem.Transform.childCount == 0)//long strip
                {
                    int correctScreen = GetMeetWithCameraScreen(screenItem.Transform);
                    screenItem.ScreenNumber = correctScreen;
                }
                else
                {
                    for (int i = 0; i < screenItem.Transform.childCount; i++)
                    {
                        Transform child = screenItem.Transform.GetChild(i);
                        if (child.name == "Tappble")
                        {
                            for (int j = 0; j < child.childCount; j++)
                            {
                                Transform tapableChild = child.GetChild(j);
                                bool isMoved = ProcessChild(screenItem, tapableChild, screenItems, "Tappble");
                                if (isMoved && j>0)
								{
									j--;
								}
                            }

                            if (child.childCount == 0)
							{
                                DestroyImmediate(child.gameObject);
							}
                        }
                        else
                        {
                            bool isMoved = ProcessChild(screenItem, child, screenItems);
                            if (isMoved && i>0)
							{
                                i--;
							}
                        }
                    }
                }
            }

            foreach(ScreenItem screenItem in screenItems)
			{
                if (screenItem.Transform.childCount == 0 && screenItem.GetComponent<SpriteRenderer>() == null)
				{
                    DestroyImmediate(screenItem.gameObject);
				}
			}
        }

        private static bool ProcessChild(ScreenItem screenItem, Transform child, List<ScreenItem> items, string nestedName = null)
        {
            int correctScreenNumber = GetMeetWithCameraScreen(child);
            if (screenItem.ScreenNumber != correctScreenNumber)
            {
                ScreenItem correctScreenItem = items.FirstOrDefault(_ => _.ScreenNumber == correctScreenNumber);
                if (correctScreenItem == null)
                {
                    correctScreenItem = CreateScreenItem(correctScreenNumber, screenItem.Transform.parent);
                    items.Add(correctScreenItem);
                }
                _movingItemsCount++;
                PutObjectTo(correctScreenItem, child, nestedName);
                return true;
            }
            return false;
        }

        private static ScreenItem CreateScreenItem(int screenNumber, Transform parent)
        {
            GameObject go = new GameObject("ScreenItem" + screenNumber);
            ScreenItem screenItem = go.AddComponent<ScreenItem>();
            screenItem.ScreenNumber = screenNumber;
            go.transform.SetParent(parent);
            go.transform.localPosition = Vector3.zero;
            return screenItem;
        }

        private static void PutObjectTo(ScreenItem screenItem, Transform child, string nestedName = null)
        {
            if (nestedName == null)
            {
                child.SetParent(screenItem.Transform);
            }
            else
            {
                Transform nestedObject = null;
                for (int i = 0; i < screenItem.Transform.childCount; i++)
                {
                    Transform tr = screenItem.Transform.GetChild(i);
                    if (tr.name == nestedName)
                    {
                        nestedObject = tr;
                        break;
                    }
                }

                if (nestedObject == null)
                {
                    GameObject go = new GameObject(nestedName);
                    nestedObject = go.transform;
                    nestedObject.transform.parent = screenItem.Transform;
                    nestedObject.transform.localPosition = Vector3.zero;
                }
                child.SetParent(nestedObject);
            }
        }

        private static int GetMeetWithCameraScreen(Transform child)
        {
            float meetXPosition = GetMeetWithCameraX(child, _currentParallaxIndex);
            return GetXScreen(meetXPosition);
        }

        private static float GetMeetWithCameraX(Transform item, int parallaxLayerIndex)
        {
            return
                (item.position.x + _parallaxManager.Camera.transform.position.x*_parallaxManager.ParallaxSpeeds[parallaxLayerIndex].x)/
                (1 - _parallaxManager.ParallaxSpeeds[parallaxLayerIndex].x);
        }

        private static int GetXScreen(float x)
        {
            foreach (ScreenBorder screenBorder in _screenBorders)
			{
                if (x < screenBorder.Transform.position.x)
				{
                    return screenBorder.ScreenNumber;
				}
			}
            throw new Exception("No screen border for position: " + x);
        }

        #endregion
    }
}
