﻿using System;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;
using System.Threading;

public class Web : MonoBehaviour
{
	//public delegate string GetPageDelegate(string url);
	
	public static string GetPage(string url)
	{
		Debug.Log("Get Page::" + url);
		string page = string.Empty;
		
		HttpWebRequest request = null;
		HttpWebResponse response = null;
		StreamReader sr = null;
		
		try
		{
			request = (HttpWebRequest)WebRequest.Create(url);
			request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
			request.ReadWriteTimeout = 30000;
			request.Timeout = 30000;
			request.Proxy = null;
			response = (HttpWebResponse)request.GetResponse();
			if (response == null)
			{
				Debug.Log("No Response");
				return page;
			}
			sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
			page = sr.ReadToEnd();
		}
		catch (ThreadAbortException ex)
		{
			page = string.Empty;
			Debug.Log("ThreadAbortException::" + url);
			Debug.Log(ex.ToString());
		}
		catch (Exception ex)
		{
			page = string.Empty;
			Debug.Log("Exception::" + url);
			Debug.Log(ex.ToString());
		}
		finally
		{
			if (request != null) request.Abort();
			if (response != null) response.Close();
			if (sr != null) sr.Close();
		}
		return page;
	}
	public static string PostPage(string url, string postdata)
	{
		//Debug.Log("Post Page::" + url);
		string page = string.Empty;
		
		HttpWebRequest request = null;
		HttpWebResponse response = null;
		StreamReader sr = null;
		try
		{
			request = (HttpWebRequest)WebRequest.Create(url);
			request.Method = "POST";
			request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
			request.ReadWriteTimeout = 50000;
			request.Timeout = 50000;
			request.Proxy = null;
			
			using (StreamWriter requestWriter = new StreamWriter(request.GetRequestStream()))
			{
				requestWriter.Write(postdata);
			}
			response = (HttpWebResponse)request.GetResponse();
			if (response == null)
			{
				Debug.Log("No Response");
				return page;
			}
			sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
			page = sr.ReadToEnd();
		}
		catch (ThreadAbortException ex)
		{
			page = string.Empty;
			Debug.Log("ThreadAbortException");
			Debug.Log(ex.ToString());
		}
		catch (Exception ex)
		{
			page = string.Empty;
			Debug.Log("Exception");
			Debug.Log(ex.ToString());
		}
		finally
		{
			if (request != null) request.Abort();
			if (response != null) response.Close();
			if (sr != null) sr.Close();
		}
		return page;
	}
	
}