﻿using UnityEngine;
using System.Collections;

public class LoadingScreenManager : MonoBehaviour {

	public GameObject	LoadAll;

	void Start () 
	{
		StartCoroutine(Wait());
	}

	IEnumerator Wait()
	{
		yield return new WaitForSeconds(1.2f);
		LoadAll.SetActive(true);
		StartCoroutine(customLoadLevelAsync("BabyDreams2Unity"));
	}
	
	IEnumerator customLoadLevelAsync(string level)
	{
		yield return new WaitForSeconds(1);
		AsyncOperation asyncOp = Application.LoadLevelAsync(level);
		yield return asyncOp;
	}
}
