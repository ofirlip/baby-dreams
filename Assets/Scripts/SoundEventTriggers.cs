﻿using System;
using System.Collections;
using UnityEngine;

public class SoundEventTriggers : MonoBehaviour  
{
	public enum SoundTriggers
	{
		ButterflyHello,
		ButterflyGreat,
		
		GoodMorningDog,
		GoodMorningPeacock,
		GoodMorningCat,
		
		GoodNightDog,
		GoodNightPeacock,
		GoodNightCat,
		
		SunTap,
		CloudTap,
		RainbowTap,
		
		FlowerRed,
		FlowerOrange,
		FlowerYellow,
		
		AnimalClick,
		AnimalNameClick,
		
		SmallBirdEgg,

		Keren3,
		ButterflyConfused,
		butterflyRandomHey,

		NBedroomSleeping,
	}
}
