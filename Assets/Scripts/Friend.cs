﻿using Assets.Scripts.Utils;
using UnityEngine;

namespace Assets.Scripts
{
	public class Friend : TapableItem
    {
        public Transform LandingPivot;
        public Transform CameraPivot;
        public float CameraFocusSize;
        public Direction ButterflyLookAt;
        public OnFriendReaction ButterflyReaction;
//		public bool AlreadyClicked;
		public AudioClip Narrator;
		public MeetingSettings GetMeetingSettings()
		{
			return new MeetingSettings
			{
				LandingPivot = LandingPivot,
				CameraPivot = CameraPivot,
				CameraFocusSize = CameraFocusSize,
				ButterflyLookAt = ButterflyLookAt,
				ButterflyReaction = ButterflyReaction,
				AlreadyClicked = AlreadyClicked
			};
		}
    }
}
