﻿
namespace Assets.Scripts
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right,
        RightUp,
        RightDown,
        LeftUp,
        LeftDown
    }
}
